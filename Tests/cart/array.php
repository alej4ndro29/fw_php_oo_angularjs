<?php

    testA();
    testB();

/**********************************************/

function testA() {
    $optionA = array();

    array_push($optionA, array(
        'product' => 5,
        'quantity' => 1
    ));
    array_push($optionA, array(
        'product' => 6,
        'quantity' => 2
    ));

    array_push($optionA, array(
        'product' => 8,
        'quantity' => 5
    ));

    $newProduct = 6;


    for ($i=0; $i < count($optionA); $i++) { 
        echo $optionA[$i]['product'] . ' ';

        if ($optionA[$i]['product'] == $newProduct) {
            echo ' dentro if ';
            $optionA[$i]['quantity']++;
        }

        echo $optionA[$i]['quantity'];
        echo '<br>';
    }

    
    echo '<pre>';
    print_r($optionA);
    echo '</pre>';

    echo 'Item: ' . $optionA[0]['product'] . '. Quantity: ' . $optionA[0]['quantity'];

    echo '<hr>';

}

function testB() {
    $optionBitem = array();
    $optionBquantity = array();

    array_push($optionBitem, 4);
    array_push($optionBquantity, 1);
    
    array_push($optionBitem, 2);
    array_push($optionBquantity, 1);

    array_push($optionBitem, 1);
    array_push($optionBquantity, 1);

    array_push($optionBitem, 3);
    array_push($optionBquantity, 1);

    array_push($optionBitem, 5);
    array_push($optionBquantity, 1);


    $newItem = 5;

    if(in_array($newItem, $optionBitem)) {
        echo 'esta en: ';
        $pos = array_search($newItem, $optionBitem);
        echo $pos;
        $optionBquantity[$pos]++;

    }

    echo '<pre> PRODUCTS: ';
    print_r($optionBitem);
    echo '</pre>';

    echo '<pre> QUANTITY: ';
    print_r($optionBquantity);
    echo '</pre>';

    echo '<hr>';

}

