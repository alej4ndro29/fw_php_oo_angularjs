<footer class="py-5 bg-dark">
    <div class="container">
        <div id="noSeleccionable" class="m-0 text-right text-white">
            <span class="btn-es">es</span> | <span class="btn-en">en</span>
        </div>
        <p class="m-0 text-right text-white">Alejandro Rodríguez</p>
    </div>