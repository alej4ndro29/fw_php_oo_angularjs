<?php

    function debug($var) {
        echo '<pre>';
            print_r($var);
        echo '</pre>';
    }
    
    /* SERVER PATH */
    define('APP_PATH', $_SERVER['DOCUMENT_ROOT']);

    /* MODEL DB PATH */
    define('MODEL_DB_PATH', $_SERVER['DOCUMENT_ROOT'] . '/model/db');

    /* MODULES PATH */
    define('MODULES_PATH' , APP_PATH . '/modules');

    /* CONFIG PATH */
    define('CONF_PATH', APP_PATH . '/config');

    /* VIEW PATH */
    define('VIEW_PATH', APP_PATH . '/view');

    /* VIEW INCLUDE PATH */
    define('VIEW_INC_PATH', VIEW_PATH . '/inc');

    /* VIEW IMG PATH */
    define('VIEW_IMG_PATH', VIEW_PATH . '/img');

    /* VIEW CSS PATH */
    define('VIEW_CSS_PATH', VIEW_PATH . '/css');

    /* VIEW JS PATH */
    define('VIEW_JS_PATH', VIEW_PATH . '/js');



    debug(MODEL_DB_PATH);
    var_dump(is_dir(MODEL_DB_PATH));

    echo '<br>/////////////////<br>';

    $dir = 'asdf';

    if(substr($dir, 0, 1) == '/') {
        echo $dir;
    } else {
        $dir = '/' . $dir;
        echo $dir;
    }