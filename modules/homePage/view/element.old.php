<div class="col-lg-4 col-md-6 mb-4">
    <div class="card">
        <img class="card-img-top img<?php echo $cont?>" id='<?php echo $row['ISBNResource']?>' src="view/img/book-img.jpg" alt="">
        <div class="card-body">
            <h4 class="card-title"><?php echo $row['nameResource'] ?></h4>
            <p class="card-text"><?php echo $row['releaseResource'] ?></p>
        </div>
        <div class="card-footer">
            <button class="readRec btn btn-outline-dark btn-block" id='<?php echo $row['IDResource']?>'>More!</button>
        </div>
    </div>
</div>
