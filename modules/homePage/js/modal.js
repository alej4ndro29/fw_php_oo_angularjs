function search() {
    $('#scroll-load-icon').remove();
    var input = $('#search-home').val();
    var type = $('#search-type').val();
    var genre = $('#search-genre').val();

    if (input == "" && type == "" && genre == "") {
        document.getElementById('search-type').classList.add('is-invalid');
        document.getElementById('search-genre').classList.add('is-invalid');
        document.getElementById('search-home').classList.add('is-invalid');
        $('#menu-search').effect('shake');
    } else {
        document.getElementById('search-type').classList.remove('is-invalid');
        document.getElementById('search-genre').classList.remove('is-invalid');
        document.getElementById('search-home').classList.remove('is-invalid');

        $.ajax({
            method: 'POST',
            // url: 'index.php?module=homePage&op=search',
            url: 'homePage/search',
            data: { typ: type, gen: genre, inp: input }
        })
            .done(function (data) {
                // console.log(data);
                data = JSON.parse(data);

                $('#contents').removeClass('row text-center');
                $('#contents').fadeOut("slow", function () {
                    $('#contents').empty();
                    if (data.length == 0) { // NO RESULTS
                        console.log('No results');
                        $('#contents').append('<h1>No results</h1> <br>');
                    } else {
                        $('#contents').append('<h1>Find results:</h1> <br>');
                        for (var i = 0; i < data.length; i++) {
                            // console.log(data[i]);
                            var element = "";
                            element = element + '<div class="card">';
                            element = element + '<div class="card-header bg-dark text-light">';
                            element = element + data[i].nameResource;
                            element = element + '</div>';
                            element = element + '<div class="card-body">';
                            element = element + '<p class="card-text">';
                            element = element + '<b>Editorial: </b>' + data[i].editorialResource + '</br>';
                            element = element + '<b>ISBN: </b>' + data[i].ISBNResource + '</br>';
                            element = element + '</p>'
                            element = element + '<button class="readRec btn btn-outline-dark" id="' + data[i].IDResource + '">Show more!</button>';
                            element = element + '</div>';
                            element = element + '</div>';
                            element = element + '</br>'

                            $('#contents').append(element);

                        }
                    }

                    $('#contents').fadeIn("slow");
                });
            });
    }
}

function autocomplete() {
    var text = $('#search-home').val();
    $.ajax({
        method: 'GET',
        //url: 'index.php?module=homePage&op=autocomplete&aux=' + text,
        url: "homePage/autocomplete/" + text,
        datatype: "json",
    })

        .done(function (data) {
            // console.log(data);
            data = JSON.parse(data);
            // console.log(data.length);
            $('#suggestions').empty();
            for (var i = 0; i < data.length; i++) {
                // console.log(data[i].ISBNResource);
                $('#suggestions').append('<div class="suggItem noSeleccionable" id="' + data[i].ISBNResource + '">' + data[i].ISBNResource + '</div>');

            }

        })
}

// var img0;
// var img1;
// var img2;

// function loadImgCod() {
//     var prLoadImg = new Promise(function (resolve, reject) {
//         $(".img0").on('load', function () {
//             img0 = (this.id);
//             $(".img1").on('load', function () {
//                 img1 = (this.id);
//                 $(".img2").on('load', function () {
//                     img2 = (this.id);
//                     resolve();
//                 });
//             });
//         });
//     });
//     prLoadImg
//     .then (
//         function () {
//             loadImg('img0', img0);
//             loadImg('img1', img1);
//             loadImg('img2', img2);
//         });
// }

// function loadImg(name, isbn) {
//     // console.log(name + isbn);
//     var url = "https://www.googleapis.com/books/v1/volumes?q=" + isbn;
//     console.log(url);
//     $.ajax({
//         method: 'GET',
//         url: url,
//         datatype: 'json'
//     })
//         .done(function (data) {
//             $("."+name).attr("src", data['items'][0]['volumeInfo']['imageLinks']['thumbnail']);
//     })
// }


$(document).ready(function () {
    // document.getElementById('search-home').select();
    // loadImgCod();

    $('#contents').on('click', '.readRec', function name() {
        sessionStorage.setItem('resID', this.id);
        window.location.href = "shop";
    })

    // SEARCH ACTIONS
    $('#search-home').on("keyup", function (e) {
        if (e.keyCode == 13) {
            $('#suggestions').empty();
            search();
        } else if ($('#search-home').val() != "") {
            autocomplete();
        } else if ($('#search-home').val() == "") {
            $('#suggestions').empty();
        }
    });

    $('#search-btn').on('click', function () {
        search();
    });

    //AUTOCOMPLETE CLICK
    $('#suggestions').on('click', '.suggItem', function () {
        // console.log($(this).attr('id'));
        document.getElementById("search-home").value = $(this).attr('id');
        $('#suggestions').empty();
        search();
    });

    //MOUSE EFFECT    
    $('#suggestions').on('mouseenter', '.suggItem',function () {
        $(this).removeClass("text-dark");
        $(this).addClass("bg-dark text-light");
    });
    
    $('#suggestions').on('mouseleave', '.suggItem',function () {
        $(this).removeClass("bg-dark text-light");
        $(this).addClass("text-dark");
        
    });

});

