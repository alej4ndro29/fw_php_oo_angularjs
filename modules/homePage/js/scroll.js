
var loading = false;
var page = 0;

window.onload = function () {
    checkVisible();
};


$(document).ready(function () {
    // checkVisible();
    $(window).scroll(function () {
        checkVisible();
    });
    
    $(window).on('resize', function () {
        checkVisible();
    });

});

function checkVisible() {
    $('#scroll-load-icon').each(function () {
        if (isScrolledIntoView($(this))) {
            if (!loading) {
                // console.log('load now');
                loading = true;
                getSuggestions();
                
            }
        }
    });
}

function isScrolledIntoView(elem){
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    if(((elemBottom <= docViewBottom) && (elemTop >= docViewTop))) {
        return true;
    } else {
        return false;
    }

}

function loadImg(id, isbn) {
    // console.log(name + isbn);
    var url = "https://www.googleapis.com/books/v1/volumes?q=" + isbn;
    // console.log(url);
    $.ajax({
        method: 'GET',
        url: url,
        datatype: 'json'
    })
        .done(function (data) {
            $("#"+id).attr("src", data['items'][0]['volumeInfo']['imageLinks']['thumbnail']);
        })

        .fail(function () {
            setTimeout(function() {
                loadImg(id, isbn)
            }, 10000);
        });

}

function getSuggestions() {
    $.ajax({
        method: 'GET',
        // url: 'index.php?module=homePage&op=getSuggestions&aux=' + page, 
        url: '/homePage/getSuggestions/' + page,  
    })
    .done(function (data) {
        // console.log(data);
        data = JSON.parse(data);

        if (data.length == 0) {
            $('#scroll-load-icon').remove();
        } else {

            for (var i = 0; i < data.length; i++) {
                //console.log(data[i]);
                var sug = "";

                sug += '<div class="col-lg-4 col-md-6 mb-4">';
                sug += '<div class="card">';
                sug += '<img class="card-img-top" id=' + data[i]['IDResource'] + ' src="view/img/book-img.jpg" alt="">';
                sug += '<div class="card-body">';
                sug += '<h4 class="card-title">' + data[i]['nameResource'] + '</h4>';
                sug += '<p class="card-text">'+  data[i]['releaseResource'] + '</p>';
                sug += '<div class="card-footer">';
                sug += '<button class="readRec btn btn-outline-dark btn-block" id=' + data[i]['IDResource'] + '>More!</button>';
                sug += '</div></div></div>';

                $('#contents').append(sug);
                loadImg(data[i]['IDResource'], data[i]['ISBNResource']);

                // console.log(data[i]);
            }
            // console.log("end");
            page++;
            loading = false;
            checkVisible();
        }
    })
    .fail(function () {
        $('#scroll-load-icon').remove();
        alert('error');
    })
}