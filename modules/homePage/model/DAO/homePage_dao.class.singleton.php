<?php
    class homePage_dao {
        static $_instance;

        private function __construct() {
        }

        public static function getInstance() {
            if(!(self::$_instance instanceof self)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }


        public function daoGETsuggestions($db) {
            $page = $_GET['aux'];
            $limitA = $page*3;
           
            return $db->createQuery("SELECT IDResource, nameResource, ISBNResource, releaseResource, dateAddedResource
                                    FROM Resources
                                    ORDER BY IDResource DESC
                                    LIMIT $limitA,3;");
                                    //ORDER BY ant
        }

        public function daoGETs_001($db) {
            $q = $_POST['inp'];

            return $db->createQuery("SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
                                    FROM Resources
                                    WHERE nameResource LIKE '$q%'  
                                    OR editorialResource LIKE '$q%'
                                    OR ISBNResource LIKE '$q%';");
        }

        public function daoGETs_010($db) {
            $q = $_POST['gen'];

            return $db->createQuery("SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
                                    FROM Resources
                                    WHERE generoResource LIKE '%$q%';");
        }

        public function daoGETs_011($db) {
            $q = $_POST['inp'];
            $qGen = $_POST['gen'];

            return $db->createQuery("SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
                                    FROM Resources
                                    WHERE generoResource LIKE '%$qGen%' 
                                    AND (nameResource LIKE '$q%'
                                    OR editorialResource LIKE '$q%'
                                    OR ISBNResource LIKE '$q%');");
        }

        public function daoGETs_100($db) {
            $q = $_POST['typ'];
            
            return $db->createQuery("SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
                                    FROM Resources
                                    WHERE typeResource = '$q';");
        }

        public function daoGETs_101($db) {
            $q = $_POST['inp'];
            $qTyp = $_POST['typ'];

            return $db->createQuery("SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource
                                    FROM Resources
                                    WHERE typeResource = '$qTyp'
                                    AND (nameResource LIKE '$q%'
                                    OR editorialResource LIKE '$q%'
                                    OR ISBNResource LIKE '$q%');");
        }

        public function daoGETs_110($db) {
            $qTyp = $_POST['typ'];
            $qGen = $_POST['gen'];

            return $db->createQuery("SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
                                    FROM Resources
                                    WHERE typeResource = '$qTyp'
                                    AND generoResource LIKE '%$qGen%';");
        }

        public function daoGETs_111($db) {
            $q = $_POST['inp'];
            $qTyp = $_POST['typ'];
            $qGen = $_POST['gen'];

            return $db->createQuery("SELECT * 
                                    FROM Resources
                                    WHERE typeResource = '$qTyp'
                                    AND generoResource LIKE '%$qGen%'
                                    AND (nameResource LIKE '$q%'
                                    OR editorialResource LIKE '$q%'
                                    OR ISBNResource LIKE '$q%');");
        }

        public function daoGETautocomplete($db) {
            $q = $_GET['aux'];
            
            return $db->createQuery("SELECT ISBNResource
                                    FROM Resources
                                    WHERE ISBNResource LIKE '$q%'
                                    ORDER BY ISBNResource
                                    LIMIT 15;");
        }

    }
    