<?php
    ////////////////////////////////////////////////////
    /*                   OLD                          */
    ////////////////////////////////////////////////////

    include __DIR__ . '/../../../utils/db.php';

    function s_001() {
        $q = $_POST['inp'];
        
        $sql = "SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
            FROM Resources
            WHERE nameResource LIKE '$q%'  
            OR editorialResource LIKE '$q%'
            OR ISBNResource LIKE '$q%';";

        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return($result);
    }

    function s_010() {
        $q = $_POST['gen'];
        
        $sql = "SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
            FROM Resources
            WHERE generoResource LIKE '%$q%';";

        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return($result);

    }

    function s_011() {
        $q = $_POST['inp'];
        $qGen = $_POST['gen'];
        
        $sql = "SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
                FROM Resources
                WHERE generoResource LIKE '%$qGen%' 
                AND (nameResource LIKE '$q%'
                OR editorialResource LIKE '$q%'
                OR ISBNResource LIKE '$q%');";

        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return($result);

    }

    function s_100() {
        $q = $_POST['typ'];
        
        $sql = "SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
                FROM Resources
                WHERE typeResource = '$q';";

        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return($result);

    }

    function s_101() {
        $q = $_POST['inp'];
        $qTyp = $_POST['typ'];
        
        $sql = "SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource
                FROM Resources
                WHERE typeResource = '$qTyp'
                AND (nameResource LIKE '$q%'
                OR editorialResource LIKE '$q%'
                OR ISBNResource LIKE '$q%');";

        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return($result);

    }

    function s_110() {
        $qTyp = $_POST['typ'];
        $qGen = $_POST['gen'];
        
        $sql = "SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
                FROM Resources
                WHERE typeResource = '$qTyp'
                AND generoResource LIKE '%$qGen%';";

        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return($result);

    }

    function s_111() {
        $q = $_POST['inp'];
        $qTyp = $_POST['typ'];
        $qGen = $_POST['gen'];

        $sql = "SELECT * 
                FROM Resources
                WHERE typeResource = '$qTyp'
                AND generoResource LIKE '%$qGen%'
                AND (nameResource LIKE '$q%'
                OR editorialResource LIKE '$q%'
                OR ISBNResource LIKE '$q%');";

        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return($result);

    }

    function autocomplete() {
        // error_log($_GET['text']);
        // die;
        $q = $_GET['text'];

        $sql = "SELECT ISBNResource
                FROM Resources
                WHERE ISBNResource LIKE '$q%'
                ORDER BY ISBNResource
                LIMIT 15;";

        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return($result);

    }