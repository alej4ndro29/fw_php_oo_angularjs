<?php
    ////////////////////////////////////////////////////
    /*                   OLD                          */
    ////////////////////////////////////////////////////

    function getList() {
        $conexion = connectDB();

        // $sql = "SELECT * FROM Resources
        //         ORDER BY antiquityResource
        //         LIMIT 3;";

        $sql = "SELECT IDResource, nameResource, ISBNResource, releaseResource, DATEDIFF(CURRENT_DATE(), dateAddedResource) ant
                FROM Resources
                ORDER BY ant
                LIMIT 3;";

        $result = mysqli_query($conexion, $sql);

        disconnectDB($conexion);

        return $result;
    }