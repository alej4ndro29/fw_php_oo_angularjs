var totalPrice = 0;

function getElementList(id) {
    
    $.ajax({
        method: 'GET',
        url: amigableUrl('cart/getElementList/' + id) ,
        datatype: "json"
    })
        .done(function (data) {
            data = JSON.parse(data);
            var title = data['name'];
            var quantity = data['quantity'];
            var price = data['price'];
            totalPrice += parseFloat(price * quantity);
            showAnElement(title, id, quantity, price);
        });
}

function showAnElement(title, prodID, quantity, price) {
    var text = "";
    text += '<div class="card-header element" id="' + prodID + '">';
    text += '<div class="container">'
    text += '<div class="row">'
    text += '<div class="col">'
    text += title;
    text += '</div>';
    text += '<div class="col-2">';
    text += price +  ' €'
    text += '</div>';
    text += '<div class="col-1 noSeleccionable">';
    text += '<span class="glyphicon glyphicon-minus minus-item pointer" id="' + prodID + '"></span>';
    text += '<span class="quantity" id="' + prodID + '"> ' + quantity + ' </span>';
    text += '<span class="glyphicon glyphicon-plus add-item pointer" id="' + prodID + '"></span>';
    text += '</div>';
    text += '<div class="col-1">';
    text += '<span class="glyphicon glyphicon-remove remove-item pointer" id="' + prodID + '"></span>';
    text += '</div></div></div>';

    $('.total-price').html(totalPrice.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
        + ' €');
    $('.cart-elements').append(text);
}

function showElements(data) {
    for (var i = 0; i < data.length; i++) {
        var prodID = data[i]['productID'];
        getElementList(prodID);
    }
}

function getList() {
    totalPrice = 0;
    $('.cart-elements').empty();
    $.ajax({
        method: 'GET',
        url: amigableUrl('cart/getList'),
        datatype: "json"
    })
    .done(function (data) {
        data = JSON.parse(data);
        if (data.length == 0) { // CARRITO VACÍO
            $('.empty-msg').attr('hidden', false);
            $('.content-cart-details').attr('hidden', true);
        } else {
            showElements(data);
        }
    });
}

function removeItem(item) {
    $.ajax({
        method: 'POST',
        url: amigableUrl('cart/removeItem'),
        data: { item: item }
    })
    .done(function (data) {
        updateCheck(data);
    });
}


function addOne(item) {
    $.ajax({
        method: 'POST',
        url: amigableUrl('cart/addOne'),
        data: { item: item }
    })
    .done(function (data) {
        updateCheck(data);
    });
}

function minusOne(item) {
    $.ajax({
        method: 'POST',
        url: amigableUrl('cart/minusOne'),
        data: { item: item }
    })
    .done(function (data) {
        updateCheck(data);
    });
}

function updateCheck(data) {
    if (data) {
        getList();
    }
    else {
        alert('An error has occurred');
    }
}



function completeOrder() {
    if (getUserToken() == null) {
        window.location.href = amigableUrl('login');
    } else {
        $.ajax({
            method: 'POST',
            url: amigableUrl('cart/completeOrder'),
            data: {userToken: getUserToken()}
        })
        .done(function (data) {
            $('#modal-info-content').empty();
            $('#modal-info-content').append('<p>Thanks.</p>');
            $('#modal-info-content').append('<p>Your order has been completed</p>');
            $('#modal-info').modal('show');
        })
        .error(function (data) {
            alert('Error processing the purchase');
        });
    }

}

$(document).ready(function () {
    getList();

    $(document).on('click', '.add-item', function () {
        addOne(this.id);
    });

    $(document).on('click', '.minus-item', function () {
        minusOne(this.id);
    });

    $(document).on('click', '.remove-item', function () {
        removeItem(this.id)
    });

    $(document).on('click', '.complete-order', function () {
        completeOrder();
    });
    
    $('#modal-info').on('hidden.bs.modal', function () {
        document.location = './';
    })

    $(document).on('click', '.redirect-to-shop', function () {
        window.location.href = amigableUrl('/order');
    });

});