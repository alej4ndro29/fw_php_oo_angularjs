<?php

    include __DIR__."/../../../utils/db.php";

    function searchItem($item) {
        
        $conexion = connectDB();

        $sql = "SELECT *
                FROM Resources
                WHERE IDResource = $item;";

        $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);

        return $result;

    }

    function getNamePrice($item) {
        
        $conexion = connectDB();

        $sql = "SELECT nameResource, price
                FROM Resources
                WHERE IDResource = $item;";

        $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);

        return $result;

    }

    function deleteCart($userID) {
        $sql = "DELETE FROM cart
                WHERE userID = $userID;";
    
        $conexion = connectDB();
        $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
    }

    function removeItem($userID, $resourceID)  {
        $sql = "DELETE FROM cart
                WHERE userID = $userID
                AND resourceID = $resourceID;";
    
        $conexion = connectDB();
        $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
    }

    function addOne($userID, $resourceID) {
        $sql = "UPDATE cart
                SET
                    quantity = quantity + 1
                WHERE userID = $userID
                AND resourceID = $resourceID ;";
    
        $conexion = connectDB();
        $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
    }

    function minusOne($userID, $resourceID) {
        $sql = "UPDATE cart
                SET
                    quantity = quantity - 1
                WHERE userID = $userID
                AND resourceID = $resourceID ;";
    
        $conexion = connectDB();
        $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
    }

    function createCart($userID, $productID, $quantity) {
        $sql = "INSERT INTO cart
                    (userID,
                    resourceID,
                    quantity)
                VALUES (
                    $userID,
                    $productID,
                    $quantity
                );";

        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
    }

    function toHistory($userID) {
        $sql = "SELECT c.*, (r.price * c.quantity) priceElement
                FROM cart c
                INNER JOIN Resources r ON c.resourceID = r.IDResource
                WHERE c.userID = $userID;";
        
        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        
        // GENERATE ID
        $purchaseID = $userID.'_'.date('d-m-Y_H:i:s');
        
        foreach ($result as $key) {
            // print_r($key);

            $resourceID = $key['resourceID'];
            $priceElement = $key['priceElement'];
            $quantity = $key['quantity'];

            $sql = "INSERT INTO historyShop (
                    userID,
                    resourceID,
                    price,
                    quantity,
                    purchaseID)
                    VALUES (
                    '$userID',
                    '$resourceID',
                    '$priceElement',
                    '$quantity',
                    '$purchaseID'
                    );";
            

            $result = mysqli_query($conexion, $sql);
        }

        disconnectDB($conexion);
    }