<?php
    if (!isset($_SESSION)) {
        session_start();
    }

    if(!isset($_SESSION['activeUser'])){
        die('<script>window.location.href = "index.php?page=login"</script>');
    }

    if (!isset($_GET['op'])) {
        include 'view/profile_view.html';
    } else {
        $path_model = __DIR__.'/model/model/';
        $path = $_SERVER['DOCUMENT_ROOT'];
        include $path.'/utils/includes/includes.php';

        switch ($_GET['op']) {
            case 'uploadAvatar':
                $result_prodpic = upload_files($_SERVER['DOCUMENT_ROOT'].'/media/userAvatar/');

                // error_log($result_prodpic['data']);
                try {
                    $user = unserialize($_SESSION['activeUser']);
                    $_SESSION['oldAvatar'] = $user->getAvatar();
                    // print_r($a[0]['avatar']);
                    // print_r($_SESSION);
                    loadModel($path_model, 'profile_model', 'post', 'updateAvatar', $result_prodpic['data']);

                    $user->setAvatar($result_prodpic['data']);
                    $_SESSION['activeUser'] = serialize($user);

                } catch (Exception $e) {
                    echo($e->getMessage());
                }
                    
                $_SESSION['result_prodpic'] = $result_prodpic;
                echo json_encode($result_prodpic);
                break;

            case 'deleteAvatar':
                // echo 'delete';
                loadModel($path_model, 'profile_model', 'post', 'updateAvatar', $_SESSION['oldAvatar']);
                $_SESSION['result_prodpic'] = array();
                $result = remove_files($_SERVER['DOCUMENT_ROOT'].'/media/userAvatar/');
                if($result === true){
                    echo json_encode(array("res" => true));
                } else {
                    echo json_encode(array("res" => false));
                }

                $user = unserialize($_SESSION['activeUser']);

                $user->setAvatar($_SESSION['oldAvatar']);
                $_SESSION['activeUser'] = serialize($user);

                break;

            case 'loadProvinces':
                try {
                    echo json_encode(loadModel($path_model, 'profile_model', 'get', 'provinces'));
                } catch (Exception $e) {
                    echo($e->getMessage());
                }
                break;

            case 'loadTown':
                try {
                    echo json_encode(loadModel($path_model, 'profile_model', 'get', 'town', $_GET['aux']));
                } catch (Exception $e) {
                    echo($e->getMessage());
                }
                break;
            
            case 'loadUserInfo':
                try {
                    echo json_encode(loadModel($path_model, 'profile_model', 'get', 'userInfo'));
                } catch (Exception $e) {
                    echo($e->getMessage());
                }
                break;
            case 'loadLikes':
                try {
                    echo json_encode(loadModel($path_model, 'profile_model', 'get', 'likes'));
                } catch (Exception $e) {
                    echo($e->getMessage());
                }
                break;
            case 'loadPurchases':
                try {
                    echo json_encode(loadModel($path_model, 'profile_model', 'get', 'purchases'));
                } catch (Exception $e) {
                    echo($e->getMessage());
                }    
                break;
            default:
                echo 'Invalid op';
                header('HTTP/1.0 400 Bad error');
                die;
                break;
        }
    }
    