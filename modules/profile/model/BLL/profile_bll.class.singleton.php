<?php
    class profile_bll {
        private $dao;
        private $db;

        static $_instance;

        private function __construct() {
            $this->dao = profile_dao::getInstance();
            $this->db = Db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self)){
                self::$_instance = new self();
            }
            return self::$_instance;
        }
 
        private function getActiveUserID() {
            if (!isset($_SESSION['activeUser'])) {
                throw new Exception('User not logged');
            }
            $user = unserialize($_SESSION['activeUser']);
            return $user->getID();
        }

        private function executeDAO($functionName, $arrAguments = '') {
            if (method_exists($this->dao, $functionName)) {
                return $this->dao->$functionName($this->db, $arrAguments);
            } else {
                throw new Exception('Not exists DAO function -> '. $functionName);
            }
        }

        /////////GET FUNCTIONS//////////////

        public function resources($arrAguments) {
            return $this->executeDAO('daoGETResources');
        }

        public function provinces($arrAguments) {
            return $this->executeDAO('daoGETprovinces');
        }

        public function town($arrAguments) {
            return $this->executeDAO('daoGETtown', $arrAguments);
        }

        public function userInfo($data) {

            if (!isset($data['userToken'])) {
                throw new Exception('No token');
            }

            $token = $data['userToken'];
            $user = User::getUserByToken($token);


            if ($user == null) {
                throw new Exception('No user');
            }

            unset($user['passwd']);
            unset($user['emailToken']);
            unset($user['appToken']);

            echo json_encode($user);
            // return $this->executeDAO('daoGETuserInfo', $user['id']);
        }

        public function likes($data) {
            if (!isset($data['userToken'])) {
                throw new Exception('No token');
            }

            $token = $data['userToken'];
            $user = User::getUserByToken($token);
            return $this->executeDAO('daoGETlikes', $user['id']);
        }

        public function purchases($data) {
            if (!isset($data['userToken'])) {
                throw new Exception('No token');
            }

            $token = $data['userToken'];
            $user = User::getUserByToken($token);
            return $this->executeDAO('daoGETpurchases', $user['id']);
        }

        public function avatar($arrAguments) {
            return $this->executeDAO('daoGETavatar', $this->getActiveUserID());
        }


        /////////////POST FUNCTIONS//////////////////////

        public function updateAvatar($arrAguments) {
            $info = array(
                "img"=>$arrAguments,
                "user"=>$this->getActiveUserID()
            );
            // error_log(print_r($info, true));
            return $this->executeDAO('daoPOSTupdateAvatar', $info);
        }
    }
    