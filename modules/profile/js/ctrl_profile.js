var likeClick = false;
var purchasesClick = false;

$(function () {
    $("#datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        yearRange: "1900:(new Date()).getFullYear()",
        maxDate: '0',
        changeMonth: true,
        changeYear: true
    });
});


$(document).ready(function () {
    
    if(getUserToken() == null) {
        window.location.href = amigableUrl('login');
    }

    setTimeout(function () {
        loadUserInfo();
    }, 500);
    
    loadProvinces();

    // START DROPZONE
    Dropzone.autoDiscover = false;
    $("#dropzone").dropzone({
        url: "modules/profile/ctrl_profile.php?op=uploadAvatar",
        addRemoveLinks: true,
        maxFileSize: 1000,
        maxFiles: 1,
        dictResponseError: "An error has occurred on the server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                //alert(response);
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({ 'right': '300px' }, 300);
                console.log(file.name);
                console.log("Response: " + response);
            });
        },
        complete: function (file) {
            if(file.status == "success"){
                //alert("El archivo se ha subido correctamente: " + file.name);
                //console.log(file);
            }
        },
        error: function (file) {
            alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            console.log(name);
            $.ajax({
                type: "POST",
                url: "modules/profile/ctrl_profile.php?op=deleteAvatar",
                data: "filename=" + name,
                success: function (data) {
                    console.log(data);
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var element2;
                    if ((element2 = file.previewElement) !== null) {
                        element2.parentNode.removeChild(file.previewElement);
                    } else {
                        return false;
                    }
                }
            });
        }
    }); // END DROPZONE

    // MY PROFILE CLICK
    $(document).on('click', '.my-profile', function () {
       $('.my-profile').addClass('active');
       $('.my-likes').removeClass('active');
       $('.my-purchases').removeClass('active');

       $('.my-profile-content').attr('hidden',false);
       $('.my-likes-content').attr('hidden',true);
       $('.my-purchases-content').attr('hidden',true);
    });

    // MY LIKES CLICK
    $(document).on('click', '.my-likes', function () {
        $('.my-profile').removeClass('active');
        $('.my-likes').addClass('active');
        $('.my-purchases').removeClass('active');

        $('.my-profile-content').attr('hidden',true);
        $('.my-likes-content').attr('hidden',false);
        $('.my-purchases-content').attr('hidden',true);

        if (!likeClick) {
            likeClick = true;
            loadLikes();
        }

    });

    // MY PURCHASES CLICK
    $(document).on('click', '.my-purchases', function () {
        $('.my-profile').removeClass('active');
        $('.my-likes').removeClass('active');
        $('.my-purchases').addClass('active');

        $('.my-profile-content').attr('hidden',true);
        $('.my-likes-content').attr('hidden',true);
        $('.my-purchases-content').attr('hidden',false);

        if (!purchasesClick) {
            purchasesClick = true;
            loadPurchases();
        }
    });

    $(document).on('click', '.redirect-to-shop', function () {
        window.location.href = './index.php?page=order';
    });

    // UPDATE PROFILE CLICK
    $(document).on('click', '#update-profile', function () {
        // updateProfile();
    });

    // USER SELECTS PROVINCE
    $('#province-select').change(function () {
        var sel = document.getElementById('province-select');
        var value = sel.value;
        
        $('#town-select').prop("disabled", true);
        $('#town-select').html('<option>- Town -</option>');

        if (value != 'null') {
            $.ajax({
                method: 'GET',
                url: amigableUrl('profile/loadTown/' + value),
                datatype: 'json'
            })
            .done(function (data) {
                $('#town-select').prop("disabled", false);
                data = JSON.parse(data);

                for (var i = 0; i < data.length; i++) {
                    $('#town-select').append('<option value="' + data[i]['poblacion'] + '">' + data[i]['poblacion'] + '</option>');
                }
                    
            })
            .fail(function (data) {
                console.log(data);
                alert('Error loading town');
            });
        }

    });

});

function loadUserInfo() {
    $.ajax({
        method: 'POST',
        url: amigableUrl('profile/loadUserInfo'),
        datatype: 'json',
        data: {userToken:getUserToken()}
    })
    .done(function (data) {
        data = JSON.parse(data);
        
        $('#nick').val(data['nickname']);
        $('#email').val(data['email']);
        $('.birthdate').val(data['birthdate']);
    })
    .error(function () {
        console.error('Error loading user info');
    })
}

function loadProvinces() {
    $.ajax({
        method: 'GET',
        url: amigableUrl('profile/loadProvinces'),
        datatype: 'json'
    })
    .done(function (data) {
        data = JSON.parse(data);
        for (var i = 0; i < data.length; i++) {
            // console.log(data[i]);
            $('#province-select').append('<option value="'+ data[i]['id'] +'">' + data[i]['nombre'] + '</option>');
        }

    })
    .error(function (data) {
        console.log(data);
        alert('An error has been ocurred');
    });
}

function loadLikes() {
    $.ajax({
        method: 'POST',
        url: amigableUrl('profile/loadLikes'), // 'modules/profile/ctrl_profile.php?op=loadLikes',
        datatype: 'json',
        data: {userToken: getUserToken()}
    })
    .done(function (data) {
        data = JSON.parse(data);
        
        $('.my-likes-content').empty();

        if (data.length == 0) {
            $('.my-likes-content').html("<p>You do not like anything :'(</p>");
        }

        for (var i = 0; i < data.length; i++) {
            $('.my-likes-content').append('<p>' + data[i]['nameResource'] + '</p>');
        }
    })
    .error(function (data) {
            console.log(data);
            alert('An error has been ocurred');
    });
}

function loadPurchases() {
    $.ajax({
        method: 'POST',
        url: amigableUrl('profile/loadPurchases'), //'modules/profile/ctrl_profile.php?op=loadPurchases',
        datatype: 'json',
        data: {userToken: getUserToken()}
    })
    .done(function (data) {
        data = JSON.parse(data);
        $('.my-purchases-content').empty();

        if (data.length == 0) {
            $('.my-purchases-content').append('<p>You have not made any purchase</p>');
            $('.my-purchases-content').append('<button class="btn btn-outline-dark btn-block redirect-to-shop">Go to store</button>');
        }

        for (var i = 0; i < data.length; i++) {
            var price = data[i]['totalPrice'];
            price = price.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
            
            $('.my-purchases-content').append('<p>ID purchase: ' + data[i]['purchaseID'] + ' price: ' + price + ' € </p>');
            
        }

    })
    .error(function (data) {
        console.log(data);
        alert('An error has been ocurred');
    });
}