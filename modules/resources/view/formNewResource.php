<h1 data-tr="New"></h1>
<!-- <?php 
    debug($_POST); 
?> -->
<form method="POST" name="newResource" id="newResource">
    <p><span data-tr="Type"></span>:

        <input type="radio" name="tipo" id="tipo" value="libro"
            <?php
                if(isset($_POST['tipo']) && $_POST['tipo'] == "libro") {
                    echo 'checked';
                }
            ?>
        > <span data-tr="Book"></span>
        <input type="radio" name="tipo" id="tipo" value="articulo"
            <?php
                if(isset($_POST['tipo']) && $_POST['tipo'] == "articulo") {
                    echo 'checked';
                }
            ?>
        > <span data-tr="Article"></span>
        <input type="radio" name="tipo" id="tipo" value="revista"
            <?php
                if(isset($_POST['tipo']) && $_POST['tipo'] == "revista") {
                    echo 'checked';
                }
            ?>
        > <span data-tr="Magazine"></span>

        <span id="erTipo" name="erTipo">
        </span>
    </p>
    <p><span data-tr="Name"></span>:
        <input type="text" name="name" id="name" placeholder="Nombre..." autocomplete="off"
            <?php
                if(isset($_POST['name'])) {
                    echo 'value="' . $_POST['name'] . '"';
                }
            ?>
        >
        <span id="erNombre" name="erNombre"></span>
    </p>
    <p><span data-tr="Editorial"></span>:
        <input type="text" name="editorial" id="editorial" placeholder="Editorial..." autocomplete="off"
            <?php
                if(isset($_POST['editorial'])) {
                    echo 'value="' . $_POST['editorial'] . '"';
                }
            ?>
        >
        <span id="erEditorial" name="erEditorial"></span>
    </p>
    <p>ISBN:
        <input type="text" name="isbn" id="isbn" placeholder="ISBN..." autocomplete="off">
        <span id="erISBN" name="erISBN">
        <?php 
            if (isset($_POST['isbn']))
                echo 'ISBN utilizado' 
        ?>
        </span> <br>
        1111111111111
    </p>
    <p><span data-tr="Release"></span> :
        <input type="text" name="lanzamiento" id="lanzamiento" placeholder="Lanzamiento..." readonly autocomplete="off"
            <?php
                if(isset($_POST['lanzamiento'])) {
                    echo 'value="' . $_POST['lanzamiento'] . '"';
                }
            ?>
        >
        <span id="erLanzamiento" name="erLanzamiento"></span>
    </p>

    <p><span data-tr="Genre"></span> :
        <input type="checkbox" name="genero[]" id="generoAccion" value="accion"
            <?php
                if(isset($_POST['genero']) && in_array("accion", $_POST['genero'])) {
                    echo 'checked';
                }
            ?>
        > <span data-tr="Action"></span> 
        <input type="checkbox" name="genero[]" id="generoAventuras" value="aventuras"
            <?php
                if(isset($_POST['genero']) && in_array("aventuras", $_POST['genero'])) {
                    echo 'checked';
                }
            ?>
        > <span data-tr="Adventures"></span> 
        <input type="checkbox" name="genero[]" id="generoTerror" value="terror"
            <?php
                if(isset($_POST['genero']) && in_array("terror", $_POST['genero'])) {
                    echo 'checked';
                }
            ?>
        > <span data-tr="Terror"></span> 
        <input type="checkbox" name="genero[]" id="generoOtro" value="otro"
            <?php
                if(isset($_POST['genero']) && in_array("otro", $_POST['genero'])) {
                    echo 'checked';
                }
            ?>
        > <span data-tr="Other"></span> 
        <span id="erGenero" name="erGenero"></span>
    </p>

    <button class="btn btn-outline-dark" data-tr="Register" name="Submit" type="button" onclick="validateResourceNew()" />
</form>