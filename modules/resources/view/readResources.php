<h1>Read</h1>
<?php
    //debug($_GET);
    include __DIR__ . '/../dao/DAOread.php';

    $res = DAOread();    
    if ($res == null || $res->num_rows == 0) {
        echo "No existe";
    } else {
        foreach ($res as $key) {
            ?>
            <table border="1">
                <tr>
                    <th>ID</th>
                    <td><?php echo $key['IDResource'] ?></td>
                </tr>
                <tr>
                    <th>Type</th>
                    <td><?php echo $key['typeResource'] ?></td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td><?php echo $key['nameResource'] ?></td>
                </tr>
                <tr>
                    <th>Editorial</th>
                    <td><?php echo $key['editorialResource'] ?></td>
                </tr>
                <tr>
                    <th>ISBN</th>
                    <td><?php echo $key['ISBNResource'] ?></td>
                </tr>
                <tr>
                    <th>Release</th>
                    <td><?php echo $key['releaseResource'] ?></td>
                </tr>
                <tr>
                    <th>Genre</th>
                    <td><?php echo $key['generoResource'] ?></td>
                </tr>
                <tr>
                    <th>Date added</th>
                    <td><?php echo $key['dateAddedResource'] ?></td>
                </tr>
                <tr>
                    <th>Popularity</th>
                    <td><?php echo $key['popularityResource'] ?></td>
                </tr>
                <tr>
                    <th>Antiquity</th>
                    <td><?php echo $key['antiquityResource'] ?></td>
                </tr>
            </table>
            <?php
            //debug($key);
        }
    }
?>

    <br>
    <a href="index.php?page=resources">Back</a>