<h1 data-tr="New"></h1>

<form method="POST" name="newResource" id="newResource">
    Type:
    <div class="form-check form-check-inline">
        <input class="form-check-input book-rad" type="radio" name="tipo" id="tipo" value="libro">
        <label data-tr="Book" id="book-lab" class="form-check-label"></label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input art-rad" type="radio" name="tipo" id="tipo" value="articulo">
        <label data-tr="Article" id="art-lab" class="form-check-label"></label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input mag-rad" type="radio" name="tipo" id="tipo" value="revista">
        <label data-tr="Magazine" id="mag-lab" class="form-check-label"></label>
    </div>
    <span id="erTipo" name="erTipo"></span>


    <div class="form-group row">
        <label data-tr="Name" class="col-sm-2 col-form-label"></label>
        <div class="col-sm-4">
            <input type="text" name="name" id="name" placeholder="Nombre..." autocomplete="off" class="form-control">
        </div>
        <span id="erNombre" name="erNombre"></span>
    </div>

    <div class="form-group row">
        <label data-tr="Editorial" class="col-sm-2 col-form-label"></label>
        <div class="col-sm-4">
            <input type="text" name="editorial" id="editorial" placeholder="Editorial..." autocomplete="off" class="form-control">
        </div>
        <span id="erEditorial" name="erEditorial"></span>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label">ISBN</label>
        <div class="col-sm-4">
            <input type="text" name="isbn" id="isbn" placeholder="ISBN..." autocomplete="off" class="form-control">
            <button class="btn btn-sm btn-outline-dark" id="isbn-search" type="button">
                <span class="spinner-border spinner-border-sm" id="loading-api-search" role="status" aria-hidden="true" hidden></span>
                Search API
            </button>
            1111111111111
  
        </div>
        <span id="erISBN" name="erISBN"></span>
    </div>


    <div class="form-group row">
        <label data-tr="Release" class="col-sm-2 col-form-label"></label>
        <div class="col-sm-4">
            <input type="text" name="lanzamiento" id="lanzamiento" placeholder="Release..." readonly autocomplete="off"
                class="form-control">
        </div>
        <span id="erLanzamiento" name="erLanzamiento"></span>
    </div>


    <p><span data-tr="Genre"></span> :
        <input type="checkbox" name="genero[]" id="generoAccion" value="accion">
        <span data-tr="Action" id="lb-act"></span>

        <input type="checkbox" name="genero[]" id="generoAventuras" value="aventuras">
        <span data-tr="Adventures" id="lb-adv"></span>

        <input type="checkbox" name="genero[]" id="generoTerror" value="terror">
        <span data-tr="Terror" id="lb-terr"></span>

        <input type="checkbox" name="genero[]" id="generoOtro" value="otro">
        <span data-tr="Other" id="lb-oth"></span>

        <span id="erGenero" name="erGenero"></span>
    </p>

    <button class="btn btn-outline-dark btn-submit col-sm-6" data-tr="Register" name="Submit" type="button"></button>
    <br>    
    <button class="btn btn-outline-danger col-sm-1" id="clear-form" type="button">Clear</button>
</form>



<!-- Modal api search -->
<div class="modal fade" id="modal-api-search" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="md-api-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <p id="md-api-ed"></p>
                <p id="md-api-rel"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-outline-dark" data-dismiss="modal" id="md-api-ok">Ok</button>
            </div>
        </div>

    </div>
</div>

</div>