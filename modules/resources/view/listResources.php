<!-- <h1 data-tr="Resources list"></h1> -->

    <?php getList(); ?>
</table>

<br>
<a href="index.php?page=resources&op=create"><img src="view/img/plus.png" alt="40" width="40" ></a>
<a data-tr="Delete all" href="index.php?page=resources&op=delete&item=all"></a>


<!-- modal window -->
<section id="modalRes">
    <div id="contentRes" hidden>
        <div id="container">
            <b>ID:</b><div id="idRes"></div><br>
            <b>Type:</b> <div id="typeRes"></div><br>
            <b>Name:</b><div id="nameRes"></div><br>
            <b>Editorial:</b><div id="editorialRes"></div><br>
            <b>ISBN:</b><div id=isbnRes></div><br>
            <b>Release:</b><div id="releaseRes"></div><br>
            <b>Genre:</b><div id="genreRes"></div><br>
            <b>Date add:</b><div id="dateAddRes"></div><br>
        </div>
    </div>
</section>

<!-- error window -->
<section id="modalError">
    <div id="contError" hidden>
        <p><b data-tr="errorGeneral"></b></p>
    </div>
</section>
