<form method="POST" name="updateResource" id="updateResource">
    <p>Tipo:
        <input type="radio" name="tipo" id="tipo" value="libro" <?php if ($type == "libro") {echo 'checked';} ?>>Libro
        <input type="radio" name="tipo" id="tipo" value="articulo" <?php if ($type == "articulo") {echo 'checked';} ?>>Artículo
        <input type="radio" name="tipo" id="tipo" value="revista" <?php if ($type == "revista") {echo 'checked';} ?>>Revista

        <span id="erTipo" name="erTipo">
        </span>
    </p>
    <p>Nombre:
        <input type="text" name="name" id="name" value="<?php echo $name ?>" placeholder="Nombre..." autocomplete="off">
        <span id="erNombre" name="erNombre"></span>
    </p>
    <p>Editorial:
        <input type="text" name="editorial" id="editorial" value="<?php echo $editorial ?>"  placeholder="Editorial..." autocomplete="off">
        <span id="erEditorial" name="erEditorial"></span>
    </p>
    <p>ISBN:
        <input type="text" name="isbn" id="isbn" value="<?php echo $isbn ?>" placeholder="ISBN..." autocomplete="off">
        <span id="erISBN" name="erISBN"></span> <br>
        1111111111111
    </p>
    <p>Lanzamiento:
        <input type="text" name="lanzamiento" id="lanzamiento" value="<?php echo $release ?>" placeholder="Lanzamiento..." readonly autocomplete="off">
        <span id="erLanzamiento" name="erLanzamiento"></span>
    </p>

    <p>Género:
        <input type="checkbox" name="genero[]" id="generoAccion" value="accion" <?php if(in_array("accion", $gen)) {echo 'checked';} ?>> Acción
        <input type="checkbox" name="genero[]" id="generoAventuras" value="aventuras" <?php if(in_array("aventuras", $gen)) {echo 'checked';} ?>> Aventuras
        <input type="checkbox" name="genero[]" id="generoTerror" value="terror" <?php if(in_array("terror", $gen)) {echo 'checked';} ?>> Terror
        <input type="checkbox" name="genero[]" id="generoOtro" value="otro" <?php if(in_array("otro", $gen)) {echo 'checked';} ?>> Otro
        <span id="erGenero" name="erGenero"></span>
    </p>

    <input name="Submit" type="button" value="Actualizar" onclick="validateResourceUpdate()" />
</form> 