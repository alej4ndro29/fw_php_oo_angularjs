<h1>Update</h1>
<?php 
    include __DIR__ . '/../dao/DAOupdate.php';

    if ($_POST != null) { // POST TIENE VALORES Y ACTUALIZA
        // $comprueba = checkISBN();
        // echo $comprueba;
        // die;

        if (!DAOcheckISBN()) {
            DAOupdateColumn();
            echo 'Completado';
            die('<script>window.location.href="index.php?page=resources";</script>');
        } else {
            die('<script>window.location = window.location.href;</script>');
        }
    }
    else { // POST NULL --> CARGA COLUMNA
        //debug($_POST);
        //debug($_GET);
        $error = false;
        
        $res = DAOloadColumn();

        if ($res->num_rows == 0) {
            $error = true;
            echo "No existe";
        } else {
            foreach ($res as $key) {
                $type = $key['typeResource'];
                $name = $key['nameResource'];
                $editorial = $key['editorialResource'];
                $isbn = $key['ISBNResource'];
                $release = $key['releaseResource'];
                //$genero = $key['generoResource'];

                $gen = explode(":", $key['generoResource']);

                // debug($key);
                
                // debug($gen);

                //die();
            }
        }

        if (!$error) {
            include __DIR__ . '/../view/formUpdateResource.php';
        }
    }
?>