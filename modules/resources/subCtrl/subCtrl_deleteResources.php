<h1>Delete</h1>
<?php
    //debug($_GET);

    include __DIR__ . '/../dao/DAOdelete.php';
    
    if($_GET['item']=='all') {
        if (isset($_GET['confirm']) && $_GET['confirm'] == true) {
            DAOdeleteAll();
            die('<script>window.location.href="index.php?page=resources";</script>');
        } else {
            include 'modules/resources/view/deleteAllResourcesConfirm.php';
        }
    } else {
        if(isset($_GET['confirm']) && $_GET['confirm'] == true) {
            DAOdelete();
            die('<script>window.location.href="index.php?page=resources";</script>');
        } else {
            include 'modules/resources/view/deleteResourcesConfirm.php';
        }

    }
