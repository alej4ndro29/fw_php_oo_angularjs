<?php

    function getList() {
        include __DIR__ . "/../dao/DAOlist.php";
        $result = DAOlist();

        if ($result->num_rows === 0) {
            echo '<p data-tr="Empty"></p>';
        } else { ?>
            <table id="listTable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th data-tr="TYPE"></th>
                        <th data-tr="NAME"></th>
                        <th data-tr="ISBN"></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr> 
                </thead>
                <tbody>
            <?php
            foreach ($result as $row) {
                echo '<tr>';
                    echo '<td>'.$row['IDResource'].'</td>';
                    echo '<td>'.$row['typeResource'].'</td>';
                    echo '<td>'.$row['nameResource'].'</td>';
                    echo '<td>'.$row['ISBNResource'].'</td>';
                    
                    echo '<td><div data-tr="Read" class="readRec" id='.$row['IDResource'].'>Read</div></td>';
                    echo '<td><a data-tr="Update" href="index.php?page=resources&op=update&item='.$row['IDResource'].'">Update</a></td>';
                    echo '<td><a data-tr="Delete" href="index.php?page=resources&op=delete&item='.$row['IDResource'].'">Delete</a></td>';
                    
                echo '</tr>';
            }
            echo '</tbody>';
        }

    }