-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: 192.168.22.249    Database: biblioCafeTest
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.36-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Resources`
--

DROP TABLE IF EXISTS `Resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Resources` (
  `IDResource` int(11) NOT NULL AUTO_INCREMENT,
  `typeResource` varchar(45) DEFAULT NULL,
  `nameResource` varchar(80) DEFAULT NULL,
  `editorialResource` varchar(80) DEFAULT NULL,
  `ISBNResource` varchar(20) DEFAULT NULL,
  `releaseResource` varchar(15) DEFAULT NULL,
  `generoResource` varchar(80) DEFAULT NULL,
  `dateAddedResource` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IDResource`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Resources`
--

LOCK TABLES `Resources` WRITE;
/*!40000 ALTER TABLE `Resources` DISABLE KEYS */;
INSERT INTO `Resources` VALUES (3,'libro','asdsdaadsasa','dasasdasd','1111111111111','2018-12-03','accion:aventuras:terror','2018-12-29'),(4,'libro','fehs','hthttht','1111155111111','2017-01-11','accion:aventuras','2019-01-03'),(5,'libro','fsdef','vsdv','1111111118111','2019-01-01','terror','2019-01-03'),(7,'libro','manolo','manolandia','1111111111123','2019-01-22','accion','2019-01-04'),(8,'libro','dsa','dsadsa','1111111111114','2019-01-21','accion','2019-01-05'),(9,'libro','Nombre','p','1111111111113','2019-01-12','accion','2019-01-05'),(10,'articulo','asd','asd','1111111111116','2019-01-01','terror','2019-01-05'),(12,'articulo','asdasdas','dassdaasd','1114111111111','2019-01-01','aventuras','2019-01-06'),(13,'articulo','fasdffd','asdfsa','1111987111111','2019-01-02','accion:terror','2019-01-07'),(14,'revista','Xandi','Xandi','1111231111111','2019-01-31','otro','2019-01-07'),(15,'articulo','Nombre','DAASD','5511111111111','2019-01-01','terror','2019-01-09'),(16,'libro','Harry Potter','asdasd','1111111113211','2019-01-02','accion:aventuras','2019-01-09'),(17,'articulo','Algo','Qwe','1111111111117','2018-11-13','accion:aventuras','2019-01-15');
/*!40000 ALTER TABLE `Resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likeRes`
--

DROP TABLE IF EXISTS `likeRes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likeRes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `idRes` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likeRes`
--

LOCK TABLES `likeRes` WRITE;
/*!40000 ALTER TABLE `likeRes` DISABLE KEYS */;
INSERT INTO `likeRes` VALUES (1,14),(2,14),(3,14),(4,14),(5,14),(6,14),(7,14),(8,14),(9,14),(10,16),(11,16),(12,16),(13,14),(14,14),(15,14),(16,14),(17,14),(18,14),(19,14),(20,14),(21,14),(22,14),(23,14),(24,14),(25,14);
/*!40000 ALTER TABLE `likeRes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07 17:58:50
