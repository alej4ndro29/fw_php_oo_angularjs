<?php
    function DAOnewResource() {
        // debug($_POST);
        $conexion = connectDB();

        $tipo = $_POST['tipo'];
        $nombre = $_POST['name'];
        $editorial = $_POST['editorial'];
        $isbn = $_POST['isbn'];
        $lanzamiento = $_POST['lanzamiento'];
        $genero = "";

        foreach ($_POST['genero'] as $key) {
                if ($genero != "") {
                    $genero = $genero . ":";
                }
                $genero = $genero . $key;
            }

        // echo $tipo.$nombre.$editorial.$isbn.$lanzamiento.$genero;

        $sql = "INSERT INTO `Resources`
                (
                    `typeResource`,
                    `nameResource`,
                    `editorialResource`,
                    `ISBNResource`,
                    `releaseResource`,
                    `generoResource`,
                    `dateAddedResource`
                )
                VALUES
                (
                    '$tipo',
                    '$nombre',
                    '$editorial',
                    '$isbn',
                    '$lanzamiento',
                    '$genero',
                    CURRENT_DATE()
                );";
        
        // echo $sql;
        // die();

        $result = mysqli_query($conexion, $sql);

        // AÑADIR PRECIO ALEATORIO
        $sql = "UPDATE Resources
                SET price = ROUND(RAND()*(20-5)+5, 2)
                WHERE price IS NULL;";
        
        $result = mysqli_query($conexion, $sql);

        disconnectDB($conexion);
    }

    function DAOcheckISBN() {
        $error = false;
        $conexion = connectDB();
        $sql = "SELECT * FROM `Resources` WHERE `ISBNResource` = " . $_POST['isbn'] . ";";
        // echo $sql;

        $result = mysqli_query($conexion, $sql);

        foreach ($result as $key) {
            // echo $key['ISBNResource'];
            if ($key['ISBNResource'] != null) {
                $error = true;
            }
        }

        disconnectDB($conexion);
        return $error;
    }