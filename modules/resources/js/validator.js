$(function () {
    $("#lanzamiento").datepicker({
        dateFormat: "yy-mm-dd",
        yearRange: "1800:(new Date()).getFullYear()",
        changeMonth: true,
        changeYear: true
    });
});

var error = false;

function clearForm() {
    $('.book-rad').prop('checked', false);
    $('.art-rad').prop('checked', false);
    $('.mag-rad').prop('checked', false);

    $('#name').val("");
    $('#editorial').val("");
    $('#isbn').val("");
    $('#lanzamiento').val("");

    $('#generoAccion').prop('checked', false);
    $('#generoAventuras').prop('checked', false);
    $('#generoTerror').prop('checked', false);
    $('#generoOtro').prop('checked', false);

    document.getElementById('erTipo').innerHTML = ('');
    document.getElementById('erNombre').innerHTML = ('');
    document.getElementById('erEditorial').innerHTML = ('');
    document.getElementById('erISBN').innerHTML = ('')
    document.getElementById('erLanzamiento').innerHTML = ('');
    document.getElementById('erGenero').innerHTML = ('');
}

function getGenero() {
    var genero = "";
    if (document.getElementById('generoAccion').checked) {
        genero = genero + "Acción";
    }

    if (document.getElementById('generoAventuras').checked) {
        if (genero != "") {
            genero = genero + ":";
        }
        genero = genero + "Aventuras";
    }

    if (document.getElementById('generoTerror').checked) {
        if (genero != "") {
            genero = genero + ":";
        }
        genero = genero + "Terror";
    }

    if (document.getElementById('generoOtro').checked) {
        if (genero != "") {
            genero = genero + ":";
        }
        genero = genero + "Otro";
    }

    return genero;
}

function validateTipo(tipo) {
    if (tipo == null) {
        //console.log("ERROR TIPO");
        document.getElementById('erTipo').innerHTML = ('Indique el tipo de recurso');
        return false;
    } else {
        document.getElementById('erTipo').innerHTML = ('');
        return true;
    }
}

function validateName(name) {
    if (name == "") {
        document.getElementById('erNombre').innerHTML = ('Se debe escribir un nombre.');
        return false;
    } else {
        document.getElementById('erNombre').innerHTML = ('');
        return true;
    }
}

function validateEditorial(editorial) {
    if (editorial == "") {
        document.getElementById('erEditorial').innerHTML = ('Se debe escribir una editorial.');
        return false;
    } else {
        document.getElementById('erEditorial').innerHTML = ('');
        return true;
    }
}

function validateISBN(isbn) {
    var ok = false;
    var regexp = /[0-9]{13}/;
    if (!regexp.test(isbn) || isbn.length != 13) {
        document.getElementById('erISBN').innerHTML = ('Se debe introducir un ISBN correcto.');
    } else {
        document.getElementById('erISBN').innerHTML = ('');
        ok = true;
    }

    return ok;
}

function validateLanzamiento(lanzamiento) {
    //var regexp = /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/; // dd/mm/yyyy
    var regexp = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/; // yyyy-mm-dd

    if (!regexp.test(lanzamiento)) {
        document.getElementById('erLanzamiento').innerHTML = ('Se debe introducir una fecha correcta.');
    } else {
        document.getElementById('erLanzamiento').innerHTML = ('');
    }

    return regexp.test(lanzamiento);
}

function validateGenero(genero) {
    if (genero == "") {
        document.getElementById('erGenero').innerHTML = ('Se debe seleccionar un género.');
        return false;
        //console.log('MOSTRAR ERROR GENERO');
    } else {
        document.getElementById('erGenero').innerHTML = ('');
        return true;
    }
}

function validateResourceNew() {
    error = false;
    var tipo = $('input[name=tipo]:checked').val();
    var name = document.getElementById('name').value;
    var editorial = document.getElementById('editorial').value;
    var isbn = document.getElementById('isbn').value;
    var lanzamiento = document.getElementById('lanzamiento').value;
    var genero = getGenero();

    var tipoOK = validateTipo(tipo);
    var nameOK = validateName(name);
    var editorialOK = validateEditorial(editorial);
    var isbnOK = validateISBN(isbn);
    var lanzamientoOK = validateLanzamiento(lanzamiento);
    var generoOK = validateGenero(genero);

    if (tipoOK && nameOK && editorialOK && isbnOK && lanzamientoOK && generoOK) {
        document.newResource.submit();
        document.newResource.action = "index.php?page=newResource.php";
    }

}

function validateResourceUpdate() {
    error = false;

    var url_string = window.location.href;
    var url = new URL(url_string);
    var item = url.searchParams.get("item");


    var tipo = $('input[name=tipo]:checked').val();
    var name = document.getElementById('name').value;
    var editorial = document.getElementById('editorial').value;
    var isbn = document.getElementById('isbn').value;
    var lanzamiento = document.getElementById('lanzamiento').value;
    var genero = getGenero();

    var tipoOK = validateTipo(tipo);
    var nameOK = validateName(name);
    var editorialOK = validateEditorial(editorial);
    var isbnOK = validateISBN(isbn);
    var lanzamientoOK = validateLanzamiento(lanzamiento);
    var generoOK = validateGenero(genero);

    if (tipoOK && nameOK && editorialOK && isbnOK && lanzamientoOK && generoOK) {
        document.getElementById('updateResource').submit();
        document.getElementById('updateResource').action = "index.php?page=resources.php&op=update";
    }

}



$(document).ready(function () {
    $(document).on('click', '#book-lab', function () {
        $('.book-rad').prop('checked', true);
    });
    $(document).on('click', '#art-lab', function () {
        $('.art-rad').prop('checked', true);
    });
    $(document).on('click', '#mag-lab', function () {
        $('.mag-rad').prop('checked', true);
    });


    $(document).on('click', '#lb-act', function () {
        if (document.getElementById('generoAccion').checked) {
            $('#generoAccion').prop('checked', false);
        } else {
            $('#generoAccion').prop('checked', true);
        }
        
    });
    $(document).on('click', '#lb-adv', function () {
        if (document.getElementById('generoAventuras').checked) {
            $('#generoAventuras').prop('checked', false);
        } else {
            $('#generoAventuras').prop('checked', true);
        }
    });

    $(document).on('click', '#lb-terr', function () {
        if (document.getElementById('generoTerror').checked) {
            $('#generoTerror').prop('checked', false);
        } else {
            $('#generoTerror').prop('checked', true);
        }
    });

    $(document).on('click', '#lb-oth', function () {
        if (document.getElementById('generoOtro').checked) {
            $('#generoOtro').prop('checked', false);
        } else {
            $('#generoOtro').prop('checked', true);
        }
    });

    $(document).on('click', '.btn-submit', function () {
        validateResourceNew()
    });

    $(document).on('click', '#clear-form', function name(params) {
        clearForm();
    });

});