var titleAPI;
var editorialAPI;
var relAPI;
var typeAPI;

function apiSearch() {
    var isbn = document.getElementById('isbn').value;
    var url = "https://www.googleapis.com/books/v1/volumes?q=" + isbn;

    if (validateISBN(isbn)) {
        console.log(url);
        $('#loading-api-search').attr('hidden', false);
        $.ajax({
            method: 'GET',
            url: url,
            datatype: 'json'
        })
            .complete(function () {
                $('#loading-api-search').attr('hidden', true);
            })

            .done(function (data) {
                if (data['totalItems'] == 0) {
                    alert('Nothing');
                } else {
                    titleAPI        = data['items']['0']['volumeInfo']['title'];
                    editorialAPI    = data['items']['0']['volumeInfo']['publisher'];
                    relAPI          = data['items']['0']['volumeInfo']['publishedDate'];
                    if (relAPI != null && relAPI.length == '4') { 
                        // SI LA API SOLO OFRECE EL AÑO LE PONEMOS UNO DE ENERO POR DEFECTO
                        relAPI = "01-01-" + relAPI;
                    }
                    typeAPI         = data['items']['0']['volumeInfo']['printType'];
                    modalApiSearchShow();
                }
            })
    }    
}

function modalApiSearchShow() {
    $('#md-api-title').empty();
    $('#md-api-ed').empty();
    $('#md-api-rel').empty();
    $('#modal-api-search').modal('show');

    $('#md-api-title').html(titleAPI);
    $('#md-api-ed').html(editorialAPI);
    $('#md-api-rel').html(relAPI);
}

function setParams() {
    $('#name').val(titleAPI);
    $('#editorial').val(editorialAPI);
    $('#lanzamiento').val(relAPI);
    if (typeAPI == 'BOOK') {
        $('.book-rad').prop('checked', true);
    }
    $('#generoOtro').prop('checked', true);
}

$(document).ready(function () {
    $('#isbn').on("keyup", function (e) {
        if (e.keyCode == 13) {
            apiSearch();
        }
    });

    $(document).on('click', '#isbn-search', function () {
        apiSearch();
    });

    $(document).on('click', '#md-api-ok', function () {
        setParams();
    });
})