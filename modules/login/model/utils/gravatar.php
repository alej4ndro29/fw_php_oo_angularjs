<?php
    function get_gravatar( $email, $s = 80, $d = 'wavatar', $r = 'g', $img = false, $atts = array() ){
        $email = trim($email);
        $email = strtolower($email);
        $email_hash = md5($email);
        
        $url = "https://www.gravatar.com/avatar/".$email_hash;
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";

        return $url;
    }