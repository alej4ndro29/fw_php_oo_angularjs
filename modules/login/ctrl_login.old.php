<?php
    if (!isset($_SESSION)) {
        session_start();
    }
    
    if (!isset($_GET['op'])) { // PINTAR USUARIO PANTALLA LOGIN
        if (isset($_SESSION['activeUser'])) {
            echo('<script>window.location.href = "./";</script>');
        }

        include 'view/main.php';
    } else {
        // include 'model/user.class.php';
        include __DIR__.'/../../components/login/user.class.php';
        include 'model/rex.php';
        include 'model/checkDB.php';
        include 'model/gravatar.php';

        switch ($_GET['op']) {
            case 'login':
                try {
                    $userDB = getUserDB($_POST['lg-nick']);
                    $userData = mysqli_fetch_assoc($userDB);
                    $pass = $userData['pass'];

                    if ($userDB->num_rows == 0) {
                        throw new Exception('err-nickNotExists');
                    }

                    if (!password_verify($_POST['lg-password'], $pass)) {
                        throw new Exception('err-badPass');
                    }

                    $user = new User($userData);
                    $_SESSION['activeUser'] = serialize($user);
                    $_SESSION['logTime'] = new DateTime('now');
                    $_SESSION['activityTime'] = new DateTime('now');

                    hasElementsOnCart($user->getID());

                    echo json_encode ('ok');
                } catch (Exception $e) {
                    echo json_encode ($e->getMessage());
                }
                break;
            case 'register':
                try {
                    if (!regFormatCheck()) {
                        throw new Exception('err-sintax');
                    }

                    if (!nickExistsCheck($_POST['reg-nick'])) {
                        throw new Exception('err-nickExists');
                    }

                    if (!emailExistsCheck($_POST['reg-email'])){
                        throw new Exception('err-emailExists');
                    }

                    $nick = $_POST['reg-nick'];
                    $email = $_POST['reg-email'];
                    $cryptPasswd = password_hash($_POST['reg-password'], PASSWORD_DEFAULT);
                    $avatar = get_gravatar($email);

                    createUser($nick, $email, $cryptPasswd, $avatar);

                    echo json_encode("ok");
                } catch (Exception $e) {
                    echo json_encode ($e->getMessage());
                }
                break;
            case 'getUser':
                // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                // $charactersLength = strlen($characters);
                // $randomString = '';
                // for ($i = 0; $i < 64; $i++) {
                //     $randomString .= $characters[rand(0, $charactersLength - 1)];
                // }
                // echo($randomString);
                if(isset($_SESSION['activeUser'])) {
                    $user = unserialize($_SESSION['activeUser']);
                    // echo $user->toString();
                    $userArray = array(
                        'nick' => $user->getNick() ,
                        'email' => $user->getEmail() ,
                        'avatar' => $user->getAvatar(),
                        'type' => $user->getType()
                    );

                    echo json_encode($userArray);
                }
                break;
            case 'logout':
                $_SESSION['activityTime'] = null;
                $_SESSION['activeUser'] = null;
                session_destroy();
                break;
            default:
                break;
        }

    }
    