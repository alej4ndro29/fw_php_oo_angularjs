<?php
    if (!isset($_GET['op'])) {
        include 'view/details.php';
    } else {
        
        $path_model = __DIR__.'/model/model/';
        $path = $_SERVER['DOCUMENT_ROOT'];
        include $path.'/utils/includes/includes.php';

        switch ($_GET['op']) {
            case 'getItem':
            //     $element = getRes();

            //     foreach ($element as $key) {
            //         echo json_encode($key);
            //     }
                try {
                    echo json_encode(loadModel($path_model, 'shop_model', 'get', 'item'));
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
                
                break;

            default:
                die;
        }
    }