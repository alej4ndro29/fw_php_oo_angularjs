<?php
    class shop {

        const PATH_MODEL = __DIR__ . '/model/model/';
        static $_instance;

        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        private function loadHead() {
            include VIEW_INC_PATH . '/headers/topPageShop.php';
            include VIEW_INC_PATH . '/header.php';
        }

        private function loadFooter() {
            include VIEW_INC_PATH . '/footer.php';
            include VIEW_INC_PATH . '/bottom.php';
        }

        public function getItem() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'shop_model', 'get', 'item'));
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        public function loadView() {
            $this->loadHead();
            include 'view/details.php';
            $this->loadFooter();
        }

    }