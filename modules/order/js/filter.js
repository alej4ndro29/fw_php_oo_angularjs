$(document).ready(function () {
    var tableData = new Array();
    var id = 0;

    $.ajax({
        method: 'GET',
        url: amigableUrl('order/getData'),
        datatype: 'json'
    })

        .done(function (data) {
            data = JSON.parse(data);
            for (var i = 0; i < data.length; i++) {
                var row = {};
                row['nameResource'] = data[i].nameResource;
                row['typeResource'] = data[i].typeResource;
                row['ISBNResource'] = data[i].ISBNResource;
                // row['cantLikes'] = data[i].likes;
                row['cantLikes'] = '<span class="likeCont" id="' + data[i].IDResource + '">' + data[i].likes + '</span>';
                row['likeButton'] = '<button class="readRec btn btn-outline-dark btn-block btn-like" id="' + data[i].IDResource + '">Like</button>';
                row['buy'] = '<button class="addCart btn btn-outline-success btn-block" id="' + data[i].IDResource + '">Add to cart</button>';
                tableData[i] = row;
                // console.log(row);
            };

            var source =
            {
                localData: tableData,
                dataType: "array",
                dataFields:
                    [
                        { name: 'nameResource', type: 'string' },
                        { name: 'typeResource', type: 'string' },
                        { name: 'ISBNResource', type: 'string' },
                        { name: 'cantLikes', type: 'string' },
                        { name: 'likeButton', type: 'string' },
                        { name: 'buy', type: 'string' }
                    ]
            }

            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#listTable').jqxDataTable ({
                width: "100%",
                pageable: true,
                pagerButtonsCount: 10,
                source: dataAdapter,
                sortable: true,
                pageable: true,
                altRows: true,
                filterable: true,
                columnsResize: true,
                pagerMode: 'advanced',
                columns: [
                    { text: 'Name', dataField: 'nameResource', width: '30%'},
                    { text: 'Type', dataField: 'typeResource', width: '10%'},
                    { text: 'ISBN', dataField: 'ISBNResource', width: '20%'},
                    { text: 'Likes', dataField: 'cantLikes', width: '10%'},
                    { text: ' ', dataField: 'likeButton', width: '10%'},
                    { text: ' ', dataField: 'buy', width: '20%'}
                ]
            })

        })

        .fail(function(){
            alert('error');
        }) 
        
    $('#listTable').on('click', '.btn-like', function () {
        var userToken = getUserToken();
        
        $('.readRec').attr('disabled', true);

        if (userToken == null) {
            window.location.href = amigableUrl('login');
        }

        id = this.id;
        $.ajax({
            method: 'POST',
            url: amigableUrl('order/newLike'),
            data: {idResource: id, userToken: userToken}
        })
        .done(function (data) {
            data = JSON.parse(data);
            
            switch (data['message']) {
                case 'addedLike':
                    $('.likeCont#'+id).html(parseInt($('.likeCont#'+id).html(), 10)+1)
                    break;
                case 'removedLike':
                    $('.likeCont#'+id).html(parseInt($('.likeCont#'+id).html(), 10)-1)
                    break;
            }
            $('.readRec').attr('disabled', false);

            setUserToken(data['newToken']);

        })
        .fail(function () {
            removeUserToken();
        });

    });

});
