function addToCart(item) {
    $('#modal-err-content').empty();

    var data;
    var token = getUserToken();

    if(token == null) {
        data = { item: item };
    } else {
        data = { item: item, token: token };
    }

    $.ajax({
        method: 'POST',
        url: amigableUrl('cart/addToCart'),
        data: data
    })
        .done(function (data) {
            console.log(data);
            if(data == 'error-item') {
                $('#modal-err-content').append('<p>An error has been ocurred.</p>');
                $('#modal-error').modal('show');
            } else {

                $('#' + item + '.addCart').html("Added");
                    
                setTimeout(function () {
                   $('#' + item + '.addCart').html("Add to cart");
                }, 1000);

            }
        });
}


$(document).ready(function () {
    $(document).on('click', '.addCart', function () {
        addToCart(this.id);
    });
});