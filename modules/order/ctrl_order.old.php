<?php

if(!isset($_SESSION)) {
    session_start();
}


if (isset($_GET['op'])) {

    include 'dao/DAO_order.php';
    include __DIR__.'/../../components/login/user.class.php';
    include 'model/checkLike.php';

    switch ($_GET['op']) {
        case 'getData':
            $alldata = getbyJS();
            $arraydata = array();
            foreach ($alldata as $key) {
                array_push($arraydata, $key);
            }
            echo json_encode($arraydata);

            break;
        
        case 'newLike':
            try {
                if(!isset($_SESSION['activeUser'])){
                    throw new Exception('noUser');
                }

                $user = unserialize($_SESSION['activeUser']);

                if (checkLiked($user->getID(), $_POST['idResource'])) {
                    newLike($user->getID(), $_POST['idResource']);
                    echo 'newLike';
                } else {
                    deleteLike($user->getID(), $_POST['idResource']);
                    echo 'deleteLike';
                }
                
            } catch (Exception $e) {
                echo($e->getMessage());
            }
                
                
            break;
        
        default:
            break;
    }
} else {
    // VISTAS DEL USUARIO
    include 'view/list.php';
}
    