<?php
   class order_model {
        private $bll;
        private $db;

        static $_instance;

        private function __construct() {
            $this->bll = order_bll::getInstance();
            $this->db = Db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self)){
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        private function funcExists($functionName) {
            if (method_exists($this->bll, $functionName)) {
                return true;
            } else {
                return false;
            }
        }

        ///////////////////////////////////////

        public function post($functionName, $arrArgument) {
            if ($this->funcExists($functionName)) {
                return $this->bll->$functionName($arrArgument);
            } else {
                throw new Exception('Not exists bll function -> '. $functionName);
            }
            
        }

        public function get($functionName, $arrArgument) {
            if ($this->funcExists($functionName)) {
                $data = $this->bll->$functionName($arrArgument);
                return $this->db->listQuery($data);
            } else {
                throw new Exception('Not exists bll function -> '. $functionName);
            }
            
        }

    }
    