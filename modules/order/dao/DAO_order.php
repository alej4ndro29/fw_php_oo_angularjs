<?php
    include __DIR__."/../../../utils/db.php";

    function getbyJS() {
        $conexion = connectDB();

        // $sql = "SELECT nameResource, typeResource, ISBNResource 
        //         FROM Resources;";
        
        // $sql = "SELECT IDResource ,nameResource, typeResource, ISBNResource, IFNULL((SELECT SUM(1)
        //             FROM likeRes lk
        //             GROUP BY idRes
        //             HAVING res.IDResource = lk.idRes),0) AS likes
        //         FROM Resources res
        //         ORDER BY likes DESC;";

        $sql = "SELECT IDResource ,nameResource, typeResource, ISBNResource, COUNT(resourceID) likes
                FROM Resources r
                LEFT JOIN likeRes l ON r.IDResource = l.resourceID
                GROUP BY IDResource;";

        $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return $result;
    }

    function likesIn($idUser, $idResource) {
        $sql = "SELECT *
                FROM likeRes
                WHERE userID = $idUser
                AND resourceID = $idResource;";
        
        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return $result;
    }

    function newLike($idUser, $idResource) {
        $conexion = connectDB();

        $sql = "INSERT INTO likeRes(
                    userID,
                    resourceID
                )
                VALUES(
                    $idUser,
                    $idResource
                );";

        $result = mysqli_query($conexion, $sql);

        disconnectDB($conexion);
    }

    function deleteLike($idUser, $idResource) {
        $sql = "DELETE FROM likeRes
                WHERE userID = $idUser
                AND resourceID = $idResource;";

        $conexion = connectDB();
            $result = mysqli_query($conexion, $sql);
        disconnectDB($conexion);
        return $result;
    }