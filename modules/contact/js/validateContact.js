function validateName() {
    var ok = false;

    var name = document.getElementById('formName').value;
    if (name.length == 0) {
        document.getElementById('formName').classList.add('is-invalid');
    } else {
        document.getElementById('formName').classList.remove('is-invalid');
        ok = true;
    }
    return ok;
}

function validateEmail() {
    var ok = false;
    var regexp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    var email = document.getElementById('formEmail').value;
    if (!regexp.test(email) || email.length == 0) {
        document.getElementById('formEmail').classList.add('is-invalid');
    } else {
        document.getElementById('formEmail').classList.remove('is-invalid');
        ok = true;
    }

    return ok;
}

function validateMessage() {
    var ok = false;

    var message = document.getElementById('formMessage').value;
    if (message.length == 0) {
        document.getElementById('formMessage').classList.add('is-invalid');
    } else {
        document.getElementById('formMessage').classList.remove('is-invalid');
        ok = true;
    }
    return ok; 
}

function mailSended() {
    document.getElementById('formName').classList.add('is-valid');
    document.getElementById('formEmail').classList.add('is-valid');
    document.getElementById('formMessage').classList.add('is-valid');
    document.getElementById('formName').disabled = false;
    document.getElementById('formEmail').disabled = false;
    document.getElementById('formMessage').disabled = false;
    document.getElementById('formName').value = '';
    document.getElementById('formEmail').value = '';
    document.getElementById('formMessage').value = '';
    
    $('.modal-title').html('Email sended');
    $('.modal-body').html('Soon you will have an answer');
    $('#modal').modal('show');
}

function sendMail() {
    var nameOK = validateName();
    var emailOK = validateEmail();
    var messageOK = validateMessage();

    if (nameOK && emailOK && messageOK) {
        $('.send').attr("disabled", true);
        var name = document.getElementById('formName').value;
        var email = document.getElementById('formEmail').value;
        var message = document.getElementById('formMessage').value;

        document.getElementById('formName').disabled = true;
        document.getElementById('formEmail').disabled = true;
        document.getElementById('formMessage').disabled = true;
        $.ajax({
            method: 'POST',
            url: 'contact/sendMail',
            datatype: 'json',
            data: {name: name, email:email, message:message}
        })

        .done(function (data) {
            console.log(data);
            mailSended();
        })

        .error(function () {
            $('.modal-title').html('Server error');
            $('.modal-body').html('Server error');
            $('#modal').modal('show');
        });
    }

}

$(document).ready(function () {
    $('.send').on('click', function name(params) {
        sendMail();
    })
})
