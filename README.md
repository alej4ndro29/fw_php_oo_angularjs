# Aplicación MVC (PHP, JS, JQuery, OO)

## Intro
- PHP app, CRUD, MVC, 

## Anotaciones
### Home
- Se pueden realizar busquedas avanzadas (ej: buscar solo los libros...)
- El buscador genérico funciona en cualquier lugar de la aplicación.
- Los elementos que aparecen son los tres más recientes.

### Resources (CRUD)
- Solo se puede acceder con un usuario administrador.
- El modal de read, aparece en todas las páginas de la tabla.
- Aparece una alerta antes de eliminar un producto.

### Order
- Aparece un botón de like que dirige a la pantalla de login en caso de no haber un usuario logueado.
- Aparece una animación al momento de añadir un elemento al carrito.

### Login
- Al añadir un usuario se comprueba si hay otro usuario con el mismo nick o la misma contraseña.
- Al pasar 5 minutos se cierra la sesión del usuario y muestra un mensaje al usuario.
- Al iniciar sesión el usuario es dirigido al home y no puede volver a la pantalla del login hasta que no cierre sesión.
- Al presionar intro en un campo automáticamente cambia al siguiente o realiza la acción si es el último. (Login/Register)

### API
- Los libros pueden ser añadidos a través de la API de Google Books.
- Para los detalles del libro, se carga la portada y los relacionados a través de la API.
- Las imagenes del home, se obtienen a través de API.

### Carrito
- El precio de los libros se genera de forma automática al crearlos.
- El carrito se guarda en $_SESSION hasta que un usuario inicia sesión. Después se guarda en la tabla cart y en $_SESSION. 
- Cuando un usuario inicia sesión se comprueba si tiene dados del carrito en base de datos para cargarlo a $_SESSION, en caso de no haber datos se cargarían a la tabla de $_SESSION.
- Al añadir un producto que ya se encuentra en el carrito, aumenta la cantidad del producto en el carrito.
- Cuando se pulsa sobre completar la compra se comprueba si un usuario ha iniciado sesión.
