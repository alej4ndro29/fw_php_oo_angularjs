<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/config/path.php';

    require_once 'router.class.singleton.php';

    require_once 'amigable.php';

    require_once APP_PATH . '/utils/upload.php';
    require_once APP_PATH . '/utils/common.inc.php';
    require_once APP_PATH . '/utils/mailgun.inc.php';
    require_once APP_PATH . '/components/login/user.class.php';
    require_once APP_PATH . '/model/db/Db.class.singleton.php';
    require_once APP_PATH . '/config/constants.php';

    // Modules
    $modulesFile = simplexml_load_file(__DIR__ . '/modules.xml');
    foreach ($modulesFile as $key) {
        $moduleName = $key->name;
        
        $modulePaths = array(
            'ctrl'  => APP_PATH.'/modules/'.$moduleName .'/ctrl_'.$moduleName.'.php',
            'BLL'   => APP_PATH.'/modules/'.$moduleName .'/model/BLL/'.$moduleName.'_bll.class.singleton.php',
            'DAO'   => APP_PATH.'/modules/'.$moduleName .'/model/DAO/'.$moduleName.'_dao.class.singleton.php',
            'model' => APP_PATH.'/modules/'.$moduleName .'/model/model/'.$moduleName.'_model.class.singleton.php',
        );

        includeIfExists($modulePaths);
    }

    // Components
    $componentsFile = simplexml_load_file(__DIR__ . '/components.xml');
    foreach ($componentsFile as $key) {
        $componentName = $key->name;

        $componentPaths = array(
            'ctrl'  => APP_PATH . '/components/' . $componentName . '/ctrl_' . $componentName . '.php',
            'BLL'   => APP_PATH . '/components/' . $componentName . '/model/BLL/' . $componentName . '_bll.class.singleton.php',
            'DAO'   => APP_PATH . '/components/' . $componentName . '/model/DAO/' . $componentName . '_dao.class.singleton.php',
            'model' => APP_PATH . '/components/' . $componentName . '/model/model/' . $componentName . '_model.class.singleton.php',
        );

        includeIfExists($componentPaths);
    }

    function includeIfExists($arrPath) {
        // Load controller
        if (file_exists($arrPath['ctrl'])) {
            include_once $arrPath['ctrl'];
        }

        // Load BLL
        if (file_exists($arrPath['BLL'])) {
            include_once $arrPath['BLL'];
        }

        // Load DAO
        if (file_exists($arrPath['DAO'])) {
            include_once $arrPath['DAO'];
        }

        // Load model
        if (file_exists($arrPath['model'])) {
            include_once $arrPath['model'];
        }
    }