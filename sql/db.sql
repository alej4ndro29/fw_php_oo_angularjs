-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: 192.168.22.250    Database: biblioCafeTest
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.36-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Resources`
--

DROP TABLE IF EXISTS `Resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Resources` (
  `IDResource` int(11) NOT NULL AUTO_INCREMENT,
  `typeResource` varchar(45) DEFAULT NULL,
  `nameResource` varchar(80) DEFAULT NULL,
  `editorialResource` varchar(80) DEFAULT NULL,
  `ISBNResource` varchar(20) DEFAULT NULL,
  `releaseResource` varchar(15) DEFAULT NULL,
  `generoResource` varchar(80) DEFAULT NULL,
  `dateAddedResource` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IDResource`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Resources`
--

LOCK TABLES `Resources` WRITE;
/*!40000 ALTER TABLE `Resources` DISABLE KEYS */;
INSERT INTO `Resources` VALUES (1,'libro','Harry Potter y la piedra filosofal','Pottermore Publishing','9781781101315','2015-12-08','accion:terror','2019-02-12'),(2,'libro','Star Wars. Amos del Lado Oscuro','Planeta MÃ©xico','9786070750328','2018-06-14','otro','2019-02-12'),(3,'libro','It','Simon and Schuster','9781501141232','2016-01-01','terror:otro','2019-02-12'),(4,'libro','El resplandor','DEBOLS!LLO','9788499899275','2012-05-10','otro','2019-02-12'),(5,'libro','Los juegos del hambre','OcÃ©ano El lado oscuro','9786074001907','2009-01-01','accion:otro','2019-02-12'),(6,'libro','El pequeÃ±o libro rojo del activista en la red','Roca Editorial','9788499188225','2015-01-22','otro','2019-02-12'),(7,'libro','El universo en una cÃ¡scara de nuez','Grupo Planeta Spain','9788498929546','2016-03-22','otro','2019-02-12'),(8,'libro','Una columna de fuego (Saga Los pilares de la Tierra 3)','PLAZA & JANES','9788401018312','2017-09-12','otro','2019-02-12'),(9,'libro','Papel y tinta','SUMA','9788491293538','2019-01-24','otro','2019-02-12'),(10,'libro','El PrÃ­ncipe de la Niebla','Grupo Planeta Spain','9788408095484','2010-07-12','aventuras','2019-02-12'),(11,'libro','Metro 2033','Grupo Planeta Spain','9788448060367','2011-07-27','otro','2019-02-12'),(12,'libro','Metro 2034','Grupo Planeta Spain','9788448060374','2011-09-20','otro','2019-02-12'),(13,'libro','Metro 2035','Minotauro','9788445006443','2019-02-05','otro','2019-02-12'),(14,'libro','EspÃ­ritu vengativo','Minotauro','9788445004425','2017-01-31','otro','2019-02-12'),(15,'libro','Legados de traiciÃ³n','Minotauro','9788445004906','2017-10-17','otro','2019-02-12'),(16,'libro','El anillo de SalomÃ³n (Bartimeo 4)','MONTENA','9788484419181','2011-11-03','otro','2019-02-12'),(17,'libro','Campos de fresas','Ediciones Sm','9788434852860','1997-01-01','otro','2019-02-13'),(18,'libro','AuronPlay, el libro','Grupo Planeta Spain','9788427042629','2016-04-05','otro','2019-02-13'),(19,'libro','Harry Potter y Las Reliquias de la Muerte','Pottermore Publishing','9781781102701','2015-12-08','otro','2019-02-13'),(20,'libro','Harry Potter y la cÃ¡mara secreta','Pottermore Publishing','9781781101322','2015-12-08','otro','2019-02-13'),(21,'libro','Star Wars: Aftermath','Random House','9781473517585','2015-09-10','otro','2019-02-14'),(23,'libro','Cuentos de terror','Editorial Norma','9789580433927','1996-01-01','terror:otro','2019-02-14'),(24,'libro','Doctor SueÃ±o','PLAZA & JANES','9788401342462','2013-11-07','terror:otro','2019-02-14'),(25,'libro','Harry Potter and the Goblet of Fire','Pottermore Publishing','9781781105672','2015-12-08','aventuras:otro','2019-02-18'),(26,'libro','El caso Hartung','Roca Editorial','9788417541842','2019-01-24','otro','2019-02-24'),(27,'libro','El arte de la guerra','GOODmood','9788862776714','2014-03-06','otro','2019-03-04');
/*!40000 ALTER TABLE `Resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `userID` int(11) NOT NULL,
  `resourceID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  KEY `fk_cart_user_idx` (`userID`),
  KEY `fk_cart_item_idx` (`resourceID`),
  CONSTRAINT `fk_cart_resource` FOREIGN KEY (`resourceID`) REFERENCES `Resources` (`IDResource`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_user` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likeRes`
--

DROP TABLE IF EXISTS `likeRes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likeRes` (
  `userID` int(5) NOT NULL,
  `resourceID` int(11) NOT NULL,
  KEY `userID` (`userID`),
  KEY `resourceID` (`resourceID`),
  CONSTRAINT `likeRes_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  CONSTRAINT `likeRes_ibfk_2` FOREIGN KEY (`resourceID`) REFERENCES `Resources` (`IDResource`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likeRes`
--

LOCK TABLES `likeRes` WRITE;
/*!40000 ALTER TABLE `likeRes` DISABLE KEYS */;
INSERT INTO `likeRes` VALUES (4,1),(4,2),(5,1),(4,1),(4,5),(4,10),(4,3),(4,4),(4,6),(4,8),(4,9);
/*!40000 ALTER TABLE `likeRes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `pass` varchar(200) NOT NULL,
  `typeUser` varchar(200) NOT NULL DEFAULT 'client',
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nickname_UNIQUE` (`nickname`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'aa','a@aa.com','$2y$10$wCWdBmkZ98P/03naFv7v5O2rT3PhO3wO3MIvjFDg0Fu.5t0iZSkPG','client','https://www.gravatar.com/avatar/d10ca8d11301c2f4993ac2279ce4b930d10ca8d11301c2f4993ac2279ce4b930?s=80&d=wavatar&r=g'),(2,'ALEJ4NDRO29','josealejandro.r.29@gmail.com','$2y$10$oRO0T6ELP8ixOwwnqHxDjOWH6I9zDzmM76MV11pOr/b4NwpMFw/By','admin','https://www.gravatar.com/avatar/2b6433436c00531ab0d48fef59ce99b32b6433436c00531ab0d48fef59ce99b3?s=80&d=wavatar&r=g'),(3,'yomogan','yomogan@gmail.com','$2y$10$FrcVKddWchsGgkgyyEs/8.Xa4ehUyWP4pfi/J.BAcaCFfGvNcqccu','client','https://www.gravatar.com/avatar/9154526c03ad3e327b28e3f1f7582e3a9154526c03ad3e327b28e3f1f7582e3a?s=80&d=wavatar&r=g'),(4,'a','a@a.com','$2y$10$mO/FwRREzR90b/1Gh4UUZe9WhAy.m..aytLa3wJfXDHOLLv5ByIaC','client','https://www.gravatar.com/avatar/d10ca8d11301c2f4993ac2279ce4b930d10ca8d11301c2f4993ac2279ce4b930?s=80&d=wavatar&r=g'),(5,'admin','admin@admin.com','$2y$10$/8DwAwp3gD/qsTs9Vb6YNuylIlvQudqMCjHV2vas9lWnnanU41bE6','admin','https://www.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d302864e1b8d34f425d19e1ee2ea7236d3028?s=80&d=wavatar&r=g');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-06 18:42:27
