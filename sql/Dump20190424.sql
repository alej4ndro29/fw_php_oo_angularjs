-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: 192.168.1.152    Database: biblioCafeTest
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.36-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Resources`
--

DROP TABLE IF EXISTS `Resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Resources` (
  `IDResource` int(11) NOT NULL AUTO_INCREMENT,
  `typeResource` varchar(45) DEFAULT NULL,
  `nameResource` varchar(80) DEFAULT NULL,
  `editorialResource` varchar(80) DEFAULT NULL,
  `ISBNResource` varchar(20) DEFAULT NULL,
  `releaseResource` varchar(15) DEFAULT NULL,
  `generoResource` varchar(80) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `dateAddedResource` varchar(15) DEFAULT '1',
  PRIMARY KEY (`IDResource`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Resources`
--

LOCK TABLES `Resources` WRITE;
/*!40000 ALTER TABLE `Resources` DISABLE KEYS */;
INSERT INTO `Resources` VALUES (1,'libro','Harry Potter y la piedra filosofal','Pottermore Publishing','9781781101315','2015-12-08','accion:terror',11.04,'2019-02-12'),(2,'libro','Star Wars. Amos del Lado Oscuro','Planeta MÃ©xico','9786070750328','2018-06-14','otro',12.55,'2019-02-12'),(3,'libro','It','Simon and Schuster','9781501141232','2016-01-01','terror:otro',14.62,'2019-02-12'),(4,'libro','El resplandor','DEBOLS!LLO','9788499899275','2012-05-10','otro',6.46,'2019-02-12'),(5,'libro','Los juegos del hambre','OcÃ©ano El lado oscuro','9786074001907','2009-01-01','accion:otro',1.43,'2019-02-12'),(6,'libro','El pequeÃ±o libro rojo del activista en la red','Roca Editorial','9788499188225','2015-01-22','otro',14.79,'2019-02-12'),(7,'libro','El universo en una cÃ¡scara de nuez','Grupo Planeta Spain','9788498929546','2016-03-22','otro',12.66,'2019-02-12'),(8,'libro','Una columna de fuego (Saga Los pilares de la Tierra 3)','PLAZA & JANES','9788401018312','2017-09-12','otro',3.92,'2019-02-12'),(9,'libro','Papel y tinta','SUMA','9788491293538','2019-01-24','otro',8.6,'2019-02-12'),(10,'libro','El PrÃ­ncipe de la Niebla','Grupo Planeta Spain','9788408095484','2010-07-12','aventuras',2.26,'2019-02-12'),(11,'libro','Metro 2033','Grupo Planeta Spain','9788448060367','2011-07-27','otro',12.48,'2019-02-12'),(12,'libro','Metro 2034','Grupo Planeta Spain','9788448060374','2011-09-20','otro',12.63,'2019-02-12'),(13,'libro','Metro 2035','Minotauro','9788445006443','2019-02-05','otro',10.71,'2019-02-12'),(14,'libro','EspÃ­ritu vengativo','Minotauro','9788445004425','2017-01-31','otro',14.66,'2019-02-12'),(15,'libro','Legados de traiciÃ³n','Minotauro','9788445004906','2017-10-17','otro',12.17,'2019-02-12'),(16,'libro','El anillo de SalomÃ³n (Bartimeo 4)','MONTENA','9788484419181','2011-11-03','otro',1.86,'2019-02-12'),(17,'libro','Campos de fresas','Ediciones Sm','9788434852860','1997-01-01','otro',13.78,'2019-02-13'),(18,'libro','AuronPlay, el libro','Grupo Planeta Spain','9788427042629','2016-04-05','otro',6.35,'2019-02-13'),(19,'libro','Harry Potter y Las Reliquias de la Muerte','Pottermore Publishing','9781781102701','2015-12-08','otro',3.38,'2019-02-13'),(20,'libro','Harry Potter y la cÃ¡mara secreta','Pottermore Publishing','9781781101322','2015-12-08','otro',10.85,'2019-02-13'),(21,'libro','Star Wars: Aftermath','Random House','9781473517585','2015-09-10','otro',1.11,'2019-02-14'),(23,'libro','Cuentos de terror','Editorial Norma','9789580433927','1996-01-01','terror:otro',14,'2019-02-14'),(24,'libro','Doctor SueÃ±o','PLAZA & JANES','9788401342462','2013-11-07','terror:otro',9.66,'2019-02-14'),(25,'libro','Harry Potter and the Goblet of Fire','Pottermore Publishing','9781781105672','2015-12-08','aventuras:otro',5.33,'2019-02-18'),(26,'libro','El caso Hartung','Roca Editorial','9788417541842','2019-01-24','otro',10.65,'2019-02-24'),(27,'libro','El arte de la guerra','GOODmood','9788862776714','2014-03-06','otro',8.26,'2019-03-04'),(28,'libro','Crooked Kingdom','Henry Holt and Company (BYR)','9781627797917','2016-09-27','otro',8.37,'2019-03-07'),(29,'libro','A Court of Thorns and Roses','Bloomsbury Publishing','9781408857878','2015-05-05','otro',2.05,'2019-03-07'),(30,'libro','A Court of Frost and Starlight','Bloomsbury Publishing','9781408890318','2018-05-01','otro',12.13,'2019-03-07'),(31,'libro','Six of Crows','Henry Holt and Company (BYR)','9781627795227','2015-09-29','otro',11.51,'2019-03-07'),(32,'libro','Deseo (TrilogÃ­a Mount 3)','VERGARA','9788417664091','2019-02-14','otro',6.17,'2019-03-07'),(33,'libro','Reina roja','Ediciones B','9788466664424','2018-11-08','otro',9.29,'2019-03-07'),(34,'libro','Yo, Julia','Editorial Planeta','9788408199175','2018-11-06','otro',12.97,'2019-03-07'),(35,'libro','Quien pierde paga (TrilogÃ­a Bill Hodges 2)','PLAZA & JANES','9788401018039','2016-09-22','otro',15.34,'2019-03-09'),(36,'libro','El enigma Turing','Ediciones Destino','9788423351527','2016-09-13','otro',9.94,'2019-03-09'),(37,'libro','A Torch Against the Night','Penguin','9781101998892','2016-08-30','otro',13.7,'2019-03-09'),(38,'libro','La Guerra de los Cielos. Volumen 2','Fernando Trujillo Sanz','9781465896377','2011-07-22','otro',15.09,'2019-03-12'),(39,'libro','La CaÃ­da de Gondolin','Minotauro','9788445006412','2019-03-05','otro',11.08,'2019-03-31'),(40,'libro','Star Wars. Desde otro punto de vista','Planeta MÃ©xico','9786070748462','2018-05-15','accion:aventuras:otro',5.07,'2019-03-31'),(41,'libro','RendiciÃ³n (Premio Alfaguara de novela 2017)','ALFAGUARA','9788499928265','2017-05-25','otro',12.72,'2019-04-01');
/*!40000 ALTER TABLE `Resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `userID` int(11) NOT NULL,
  `resourceID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  KEY `fk_cart_user_idx` (`userID`),
  KEY `fk_cart_item_idx` (`resourceID`),
  CONSTRAINT `fk_cart_resource` FOREIGN KEY (`resourceID`) REFERENCES `Resources` (`IDResource`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_user` FOREIGN KEY (`userID`) REFERENCES `usersOLD` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historyShop`
--

DROP TABLE IF EXISTS `historyShop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historyShop` (
  `userID` int(11) NOT NULL,
  `resourceID` int(11) NOT NULL,
  `price` float NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `purchaseID` varchar(64) NOT NULL,
  KEY `resourceID` (`resourceID`),
  KEY `userID` (`userID`),
  CONSTRAINT `historyShop_ibfk_1` FOREIGN KEY (`resourceID`) REFERENCES `Resources` (`IDResource`),
  CONSTRAINT `historyShop_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `usersOLD` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historyShop`
--

LOCK TABLES `historyShop` WRITE;
/*!40000 ALTER TABLE `historyShop` DISABLE KEYS */;
INSERT INTO `historyShop` VALUES (4,3,29.24,2,'4_17-03-2019_18:54:23'),(4,1,11.04,1,'4_17-03-2019_18:54:23'),(4,2,12.55,1,'4_17-03-2019_18:54:23'),(4,4,6.46,1,'4_17-03-2019_18:55:28'),(4,5,1.43,1,'4_17-03-2019_18:55:28'),(4,1,11.04,1,'4_20-03-2019_18:36:13'),(4,3,14.62,1,'4_20-03-2019_18:36:13'),(4,11,12.48,1,'4_28-03-2019_17:48:06'),(4,13,10.71,1,'4_28-03-2019_17:48:06'),(4,12,12.63,1,'4_28-03-2019_17:48:06'),(1,23,14,1,'1_28-03-2019_17:48:46');
/*!40000 ALTER TABLE `historyShop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likeRes`
--

DROP TABLE IF EXISTS `likeRes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likeRes` (
  `userID` int(5) NOT NULL,
  `resourceID` int(11) NOT NULL,
  KEY `userID` (`userID`),
  KEY `resourceID` (`resourceID`),
  CONSTRAINT `likeRes_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `usersOLD` (`id`),
  CONSTRAINT `likeRes_ibfk_2` FOREIGN KEY (`resourceID`) REFERENCES `Resources` (`IDResource`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likeRes`
--

LOCK TABLES `likeRes` WRITE;
/*!40000 ALTER TABLE `likeRes` DISABLE KEYS */;
INSERT INTO `likeRes` VALUES (2,1),(2,2),(2,7),(2,6),(2,5),(4,34),(4,32),(4,33),(4,29),(4,28),(4,31),(4,24),(4,25),(4,26),(4,23),(4,37),(1,21);
/*!40000 ALTER TABLE `likeRes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` varchar(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `nickname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `emailToken` varchar(255) DEFAULT NULL,
  `appToken` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `typeUser` varchar(255) NOT NULL DEFAULT 'client',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('local|alej4',1,'alej4','josealejandro.r.29@gmail.com','','','','admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersOLD`
--

DROP TABLE IF EXISTS `usersOLD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersOLD` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `pass` varchar(200) NOT NULL,
  `birthdate` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `typeUser` varchar(200) NOT NULL DEFAULT 'client',
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nickname_UNIQUE` (`nickname`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersOLD`
--

LOCK TABLES `usersOLD` WRITE;
/*!40000 ALTER TABLE `usersOLD` DISABLE KEYS */;
INSERT INTO `usersOLD` VALUES (1,'aa','a@aa.com','$2y$10$wCWdBmkZ98P/03naFv7v5O2rT3PhO3wO3MIvjFDg0Fu.5t0iZSkPG',NULL,NULL,NULL,NULL,'client','https://www.gravatar.com/avatar/d10ca8d11301c2f4993ac2279ce4b930d10ca8d11301c2f4993ac2279ce4b930?s=80&d=wavatar&r=g'),(2,'ALEJ4NDRO29','josealejandro.r.29@gmail.com','$2y$10$oRO0T6ELP8ixOwwnqHxDjOWH6I9zDzmM76MV11pOr/b4NwpMFw/By',NULL,NULL,NULL,NULL,'admin','https://www.gravatar.com/avatar/2b6433436c00531ab0d48fef59ce99b32b6433436c00531ab0d48fef59ce99b3?s=80&d=wavatar&r=g'),(3,'yomogan','yomogan@gmail.com','$2y$10$FrcVKddWchsGgkgyyEs/8.Xa4ehUyWP4pfi/J.BAcaCFfGvNcqccu',NULL,NULL,NULL,NULL,'client','https://www.gravatar.com/avatar/9154526c03ad3e327b28e3f1f7582e3a9154526c03ad3e327b28e3f1f7582e3a?s=80&d=wavatar&r=g'),(4,'a','a@a.com','$2y$10$mO/FwRREzR90b/1Gh4UUZe9WhAy.m..aytLa3wJfXDHOLLv5ByIaC',NULL,NULL,NULL,NULL,'client','/media/userAvatar/4-608017240-Captura.PNG'),(5,'admin','admin@admin.com','$2y$10$/8DwAwp3gD/qsTs9Vb6YNuylIlvQudqMCjHV2vas9lWnnanU41bE6',NULL,NULL,NULL,NULL,'admin','/media/userAvatar/5-1193380006-Captura.PNG');
/*!40000 ALTER TABLE `usersOLD` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-24 20:37:57
