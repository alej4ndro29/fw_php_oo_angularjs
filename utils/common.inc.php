<?php
  function loadModel($model_path, $model_name, $functionType, $functionName, $arrArgument = ''){
    $model = $model_path . $model_name . '.class.singleton.php';

    if (file_exists($model)) {
        include_once($model);
       
        $modelClass = $model_name;

        if (!method_exists($modelClass, $functionType)){
            throw new Exception('Not exists model function -> '. $functionType);
        } else {
            $obj = $modelClass::getInstance();
            return $obj->$functionType($functionName, $arrArgument);
        }
        
    } else {
        throw new Exception('Not exists model file -> '. $model);
    }
  }
