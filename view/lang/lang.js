function changeLang(lang) {
  // Habilita las 2 siguientes para guardar la preferencia.
  lang = lang || localStorage.getItem('app-lang') || 'en';
  localStorage.setItem('app-lang', lang);

  // console.log(localStorage.getItem('app-lang'));

  var path = location.protocol + '//' + window.location.hostname + "/view/lang/translates/" + localStorage.getItem('app-lang') + ".json";

  $.getJSON(path, function (languajePhrases) {
    // console.log(languajePhrases);

    var elems = document.querySelectorAll('[data-tr]');

    for (var x = 0; x < elems.length; x++) {
      elems[x].innerHTML = languajePhrases
        ? languajePhrases[elems[x].dataset.tr]
        : elems[x].dataset.tr;
    }
  });
}

function amigableUrl(path) {
  if (path.startsWith('/')) {
    return location.protocol + '//' + window.location.hostname + path;
  } else {
    return location.protocol + '//' + window.location.hostname + '/' + path;  
  }
}

$(document).ready(function () {
  changeLang();

  $(".btn-es").on("click", function () {
    // console.log('esClick');
    changeLang('es');
  })

  $(".btn-en").on("click", function () {
    // console.log('enClick');
    changeLang('en');
  })
});

