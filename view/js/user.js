function setUserToken(token) {
    localStorage.setItem('userToken', token);
}

function getUserToken() {
    return localStorage.getItem('userToken');
}

function removeUserToken() {
    localStorage.removeItem('userToken');
}

function resetUserToken() {
    removeUserToken();
    location.reload();
}

function updateMenu() {
    var data = {userToken: getUserToken()};

    $.ajax({
        method: 'POST',
        url: amigableUrl('login/getUserByToken'), // 'modules/login/ctrl_login.php?op=getUser',
        datatype: 'json',
        data: data
    })

    .done(function (data) {
        data = JSON.parse(data);
        if(data['typeUser'] == null) {
            resetUserToken();
        } else {
            setUserToken(data['token']);
        }

        
        
        if (data == "") { // MOSTRAR LOGIN MENU
            $('#login-menu').attr("hidden", false);
            $('#logout-menu').attr("hidden", true);
            $('#resources-view-header').attr("hidden", true);
        } else { // MOSTRAR LOGOUT MENU
            // console.log(data);
            $('#login-menu').attr("hidden", true);
            $('#logout-menu').attr("hidden", false);
            $('#header-user-nick').html(data['nick']);
            $('#header-user-avatar').attr("src", data['avatar']);
            $('#header-user-avatar-drop').attr("src", data['avatar']);
            // $('#resources-menu-header').attr("hidden", false); // RESOURCES MENU
            if (data['typeUser'] == 'admin') {
                $('#resources-view-header').attr("hidden", false);
            }
        }
    });
}

$(document).ready(function () {

    if (localStorage.getItem('userToken') != null) {
        updateMenu();
    }

    $(document).on('click', '#logout-action', function () {
        $.ajax({
            method: 'get',
            url: amigableUrl('login/logout'),
            datatype: 'json',
        })
        .done(function name(data) {
            localStorage.removeItem('userToken');
            location.reload();  
        });

    });

});