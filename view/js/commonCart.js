$(document).ready(function () {

    $('.cart-menu').click(function (e) {
        e.stopPropagation();
    });

    $('.complete-order-btn').on('click', function () {
        window.location.href = '/cart';
    })

    $('.destroy-cart-btn').on('click', function () {
        $.ajax({
            method: 'GET',
            url: 'modules/cart/ctrl_cart.php?op=cartDestroy',
        })
        
        .done(function () {
            $('.destroy-cart-btn').html('Deleted');
            setTimeout(function() {
                $('.destroy-cart-btn').html('Destroy cart');
              }, 1000);
        });

    });


});
