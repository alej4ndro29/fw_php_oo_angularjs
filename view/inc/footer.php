<footer class="py-2 bg-dark fixed-bottom noSeleccionable">
    <div class="container">
        <div class="m-0 text-right text-white pointer">
            <span class="btn-es">es</span> | <span class="btn-en">en</span>
        </div>
        <p class="m-0 text-right text-white">Alejandro Rodríguez</p>
    </div>