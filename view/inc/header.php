<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top noSeleccionable">
    <div class="container">
        <a class="navbar-brand" href="homePage">Application</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
            aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>



        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" data-tr="Home" href="homePage"></a>
                </li>

                <li class="nav-item dropdown" id="resources-menu-header">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Resources
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" id="resources-view-header" data-tr="Resources"
                            href="/resources" hidden>Resources</a>
                        <a class="dropdown-item" data-tr="Order" href="/order"></a>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-tr="Contact" href="/contact"></a>
                </li>
            </ul>
            <div class="form-inline my-2 my-lg-0">
                <ul class="navbar-nav mr-auto">

                    <!-- SEARCH -->
                    <input class="form-control mr-sm-2 inp-nav-search" placeholder="Search">

                    <!-- CART -->
                    <div class="dropdown">
                        <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                        </button>
                        <div class="dropdown-menu cart-menu" aria-labelledby="dropdownMenuButton">
                            <div class="complete-order">
                                <button type="button" class="btn btn-outline-success btn-block complete-order-btn">
                                    Complete order
                                </button>                                
                                <button type="button" class="btn btn-outline-danger btn-block destroy-cart-btn">
                                    Cart destroy
                                </button>   
                            </div>
                        </div>
                    </div>

                    <!-- LOGIN -->
                    <li class="nav-item" id="login-menu">
                        <a class="nav-link" href="/login">Login</a>
                    </li>


                    <!-- USER AND LOGOUT -->
                    <div class="btn-group" id="logout-menu" hidden>
                        <div class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <img src="" id="header-user-avatar" width="24" height="24">
                            <span id="header-user-nick"></span>
                        </div>

                        <div class="dropdown-menu">
                            <img src="" id="header-user-avatar-drop">
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/profile">Profile</a>
                            <button class="dropdown-item" id="logout-action">Logout</button>
                        </div>
                        
                    </div>

                </ul>
            </div>
        </div>

    </div>
</nav>