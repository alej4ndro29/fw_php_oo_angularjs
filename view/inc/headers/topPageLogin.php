<!DOCTYPE html>
<html>

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#343a40">

    <title>App</title>

    <!-- Bootstrap core JavaScript -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo amigable("view/js/bootstrap.bundle.min.js")?>"></script>

    <script src="<?php echo amigable("view/lang/lang.js")?>"></script>
    <script src="<?php echo amigable("view/js/user.js")?>"></script>
    <script src="<?php echo amigable("components/search/js/ctrl_search.js")?>"></script>
    <script src="<?php echo amigable("modules/login/js/ctrl_login.js")?>"></script>
    <script src="<?php echo amigable("view/js/commonCart.js")?>"></script>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo amigable("view/css/bootstrap.min.css")?>" rel="stylesheet">
    <link href="<?php echo amigable("view/css/bootstrap-glyphicons.css")?>" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <!-- AW font -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <script src="<?php echo amigable("view/js/jquery.bootpag.min.js")?>"> </script>

    <!-- Auth0 lib -->
    <script src="https://cdn.auth0.com/js/auth0/9.2.2/auth0.min.js"></script>

    <!-- Custom styles for this template -->
    <link href="<?php echo amigable("view/css/heroic-features.css")?>" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo amigable("view/img/icon.png")?>">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="<?php echo amigable("view/css/style.css")?>" rel="stylesheet">

  </head>

  <body>