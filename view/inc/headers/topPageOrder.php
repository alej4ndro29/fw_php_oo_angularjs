<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="theme-color" content="#343a40">

  <title>App</title>

  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="view/js/bootstrap.bundle.min.js"></script>

  <script src="components/search/js/ctrl_search.js"></script>
  <script src="view/js/user.js"></script>

  <script src=view/lang/lang.js></script>
  <script src="https://jqwidgets.com/public/jqwidgets/jqx-all.js"></script>
  <script src="modules/order/js/filter.js"></script>
  <script src="modules/order/js/cart.js"></script>
  <script src="view/js/commonCart.js"></script>

  <!-- Bootstrap core CSS -->
  <link href="view/css/bootstrap.min.css" rel="stylesheet">
  <link href="view/css/bootstrap-glyphicons.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

  <script src="view/js/jquery.bootpag.min.js"></script>

  <!-- Custom styles for this template -->
  <link href="view/css/heroic-features.css" rel="stylesheet">
  <link rel="icon" type="image/png" sizes="192x192" href="view/img/icon.png">

  <link href="view/css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://jqwidgets.com/public/jqwidgets/styles/jqx.base.css">
</head>

<body>