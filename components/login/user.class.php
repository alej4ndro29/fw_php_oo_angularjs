<?php
    require 'jwt/JWT.php';
    use \Firebase\JWT\JWT;

    class User {

        const LOGIN_PATH = APP_PATH . '/modules/login/model/model/';

        private $id;
        private $nick;
        private $email;
        private $avatar;
        private $type;
        

        function __construct($data) {
            $this->id = $data['id'];
            $this->nick = $data['nickname'];
            $this->email = $data['email'];
            $this->avatar = $data['avatar'];
            $this->type = $data['typeUser'];
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        public function __set($property, $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }

        public function isAdmin() {
            if ($this->type == 'admin') {
                return true;
            } else {
                return false;
            }
            
        }

        public function updateAppToken($currentToken) {

            $user = self::getUserByToken($currentToken);

            $newToken = self::generateAppToken($user['nickname']);
            
            $data = array(
                'oldToken' => $currentToken,
                'newToken' => $newToken
            );
            loadModel(self::LOGIN_PATH, 'login_model', 'post', 'setNewToken', $data);

            return $newToken;
        }

        public function generateAppToken($tokenApp = null) {
            $key = $_SESSION['TK_KEY'];
            
            $token = array(
                'id0' => uniqid(),
                'id1' => uniqid()
            );

            if ($tokenApp != null) {
                $token['user'] = $tokenApp;
            }

            $jwt = JWT::encode($token, $key, 'HS256');

            return $jwt;
        }

        public function getUserByToken($token) {
            $data = array(
                'userToken' => $token
            );

            return loadModel(self::LOGIN_PATH, 'login_model', 'post', 'getUser', $data);
        }

        public function generateEmailToken($seed) {
            return md5(uniqid($seed));
        }

        public function generateEmailUrl($token) {
            return SITE_NAME . '/login/activate/'.$token;
        }

        public function generateRecpasswdUrk($token) {
            return SITE_NAME . '/login/recover/' . $token;
        }

    }