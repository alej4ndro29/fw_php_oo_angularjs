<?php

    class search_dao {
        static $_instance;

        private function __construct() {
        }

        public static function getInstance() {
            if(!(self::$_instance instanceof self)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }


        public function daoGETsearch($db) {
            $q = $_POST['inp'];
            $limitPerPage = $_POST['limitPerPage'];

            if($_POST['page'] == 1) {
                $limit = 0;
            } else {
                $limit = $_POST['page'] * $limitPerPage - $limitPerPage;
            }

            return $db->createQuery("SELECT IDResource, nameResource, typeResource, editorialResource, ISBNResource 
                                    FROM Resources
                                    WHERE nameResource LIKE '$q%'  
                                    OR editorialResource LIKE '$q%'
                                    OR ISBNResource LIKE '$q%'
                                    LIMIT $limit, $limitPerPage;");
        }

        public function daoGETsearchAll($db) {
            $q = $_POST['inp'];

            return $db->createQuery("SELECT count(*) cant
                                    FROM Resources
                                    WHERE nameResource LIKE '$q%'  
                                    OR editorialResource LIKE '$q%'
                                    OR ISBNResource LIKE '$q%';");
        }

    }
    
    