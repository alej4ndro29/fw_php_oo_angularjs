var input;
var limitPerPage = 3;

$(document).ready(function () {

    $('.inp-nav-search').on("keyup", function (e) {
        if (e.keyCode == 13) {
            generalSearch();
        }
    });

    $(document).on('click', '.moreDetails', function () {
        moreDetails(this.id);
    });

});

function moreDetails(id) {
    sessionStorage.setItem('resID', id);
    window.location.href = "shop";
}

function generalSearch() {
    input = $('.inp-nav-search').val();
    if (input == "") {
        // $('.inp-nav-search').effect('shake');
        $('.inp-nav-search').addClass("is-invalid");
    } else {
        $('.inp-nav-search').removeClass("is-invalid");
        $('#cont').fadeOut("slow", function () {
            calculatePages();
        });
        $('#cont').fadeIn("slow");
    }
}

function calculatePages() {
    $.ajax({
        method: 'POST',
        // url: 'components/search/ctrl_search.php?op=searchAll',
        url: amigableUrl('/component/search/searchAll'),
        data: { inp: input }
    })
    .done(function (data) {
        data = JSON.parse(data);
        pages = data[0]['cant'] / limitPerPage;

        $('#cont').html('<div id="results"></div><div class="pagination"></div>');
        if (pages == 0) {
            $('#results').append('<h1>No results</h1> <br>');
        } else {
            if (pages > 0 && pages < limitPerPage) {
                pages = 1;
            }
            showPagination(pages);    
        }
    });
    
}

function showPagination(pages) {
    loadContent(1);

    $(".pagination").bootpag({
        total: pages,
        page: 1,
        maxVisible: 5,
        next: ' >> ',
        prev: ' << '
    }).on("page", function (e, num) {
        e.preventDefault();
        loadContent(num);
    });
}

function loadContent(num) {
    $.ajax({
        method: 'POST',
        // url: 'components/search/ctrl_search.php?op=search',
        url: amigableUrl('/component/search/search'),
        data: { 'inp': input, 'page': num, 'limitPerPage': limitPerPage }
    })
    .done(function (data) {
        data = JSON.parse(data);

        $('#results').html('<h1>Find results:</h1> <br>');
        for (var i = 0; i < data.length; i++) {
            //console.log(data[i]);
            var element = "";
            element = element + '<div class="card" id="itemSearch">';
            element = element + '<div class="card-header bg-dark text-light">';
            element = element + data[i].nameResource;
            element = element + '</div>';
            element = element + '<div class="card-body">';
            element = element + '<p class="card-text">';
            element = element + '<b>Editorial: </b>' + data[i].editorialResource + '</br>';
            element = element + '<b>ISBN: </b>' + data[i].ISBNResource + '</br>';
            element = element + '</p>'
            element = element + '<button class="moreDetails btn btn-outline-dark" id="' + data[i].IDResource + '">Show more!</button>';
            element = element + '</div>';
            element = element + '</div>';
            element = element + '</br>'

            $('#results').append(element);

        }

    });
}