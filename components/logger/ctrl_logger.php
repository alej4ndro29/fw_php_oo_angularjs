<?php
class Logger {

    const FILE_PATH = 'components/logger/log/log.log';
    static $_instance;

    function __construct() {}

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public static function info($message) {
        $message = '[INFO] ' . date('d/m/Y H:i:s') . ' ' . $message . "\n";
        file_put_contents(self::FILE_PATH, $message, FILE_APPEND | LOCK_EX);
    }

    public static function warning($message) {
        $message = '[WARNING] ' . date('d/m/Y H:i:s') . ' ' . $message . "\n";
        file_put_contents(self::FILE_PATH, $message, FILE_APPEND | LOCK_EX);
    }

}
